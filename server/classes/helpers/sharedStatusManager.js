'use strict';
const config = require('../../config.js');
const fs = require('fs');
const broker = require('../broker/broker.js');
const path = require('path');

module.exports = (() => {

	/** 
 	* @function '_setDeviceAlive' - recupera il file di config e crea un oggetto da inviare
 	* @param {string} id - id del device
	* @returns {object} oggetto config da spedire
 	*/
	 var _setDeviceAlive = (id) => {
		
		//cerco se questo device è già censito
		var trovato=false;
		for (var i=0;i<global.onlineDevice.length;i++)
		{
			if (global.onlineDevice[i].deviceId==id)
			{
				trovato=true;
				global.onlineDevice[i].timestamp = Date.now();
				global.onlineDevice[i].online = true;
			}
		}
		if (!trovato)
		{
			var temp=new Object();
			temp.deviceId=id;
			temp.timestamp= Date.now();
			temp.online = true;
			global.onlineDevice.push(temp);
		}

	}		

	var _setDeviceInit = (oggetto) => {
		
		//cerco se questo device è già censito
		var trovato=false;
		for (var i=0;i<global.onlineDevice.length;i++)
		{
			if (global.onlineDevice[i].deviceId==oggetto.deviceId)
			{
				trovato=true;
				global.onlineDevice[i].timestamp = Date.now();
				global.onlineDevice[i].online = true;
				global.onlineDevice[i].name = oggetto.name;
				global.onlineDevice[i].type = oggetto.type;
				global.onlineDevice[i].role = oggetto.role;
				console.log("Device re-inizializato:");
				console.log(global.onlineDevice[i]);
			}
		}
		if (!trovato)
		{
			var temp=new Object();
			temp.deviceId=oggetto.deviceId;
			temp.timestamp= Date.now();
			temp.online = true;
			temp.name = oggetto.name;
			temp.type = oggetto.type;
			temp.role = oggetto.role;
			console.log("Nuovo Device inizializato:");
			console.log(temp);
			global.onlineDevice.push(temp);
		}

	}		


	var _checkDeviceAlive = () => {
		
		for (var i=0;i<global.onlineDevice.length;i++)
		{
			var dateLimit=Date.now()-10000; //10 secondi fa
			if (global.onlineDevice[i].timestamp<dateLimit) //non ho ricevuto aggiornamenti negli ultimi 10 secondi, lo considero offline
			{
				global.onlineDevice[i].online = false;
			}
		}
		// e alla fine mando il dettaglio a tutti i device connessi
		console.log("Shared Status:");
		console.log(global.onlineDevice);
		broker.publish('PUBLISHER_SHARED_STATUS',global.onlineDevice);
	}	

	var _sendData4Id = (id) => {
		
		//ho dei dati per questo id da mandare?
		for (var i=0;i<global.storedData.length;i++)
		{
			if (global.storedData[i].target==id)
			{
				var obj= new Object();
				obj.target=id;
				obj.data=global.storedData[i].data;
				broker.publish('PUBLISHER_DATA',obj);
			}
		}
	}	
	

	return {
		setDeviceAlive:_setDeviceAlive,
		setDeviceInit:_setDeviceInit,
		checkDeviceAlive:_checkDeviceAlive,
		sendData4Id:_sendData4Id
	}


})();