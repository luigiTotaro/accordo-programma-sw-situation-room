'use strict';
const config = require('../../config.js');
const fs = require('fs');
const broker = require('../broker/broker.js');
const path = require('path');

module.exports = (() => {

	/** 
 	* @function 'createConfig' - recupera il file di config e crea un oggetto da inviare
 	* @param {string} id - id del device
	* @returns {object} oggetto config da spedire
 	*/
	 var _createConfig = (id, experience) => {
		
		let _root_config_folder=null;
		if (experience=="") _root_config_folder = path.join(__dirname, '..', '..', "configFiles/default");
		else _root_config_folder = path.join(__dirname, '..', '..', "configFiles/"+experience);
		let _fileConfig = path.join(_root_config_folder, 'config_'+id+'.json');

		fs.readFile(_fileConfig, 'utf8', (err, data) => {
			var configObj=new Object();
			if (err == null)
			{
				try {
					configObj=JSON.parse(data);
				}
				catch(err2) {
					console.log("Errore nel parse del config per il device: "+id);
				}
			}
			
			configObj.deviceId=id;
			broker.publish('PUBLISHER_CONFIG_REPLY',configObj);

			//se questo device non è slave, comunico a tutti gli slave la scelta della experience 
			if (configObj.HWSection[0].role!="SLAVE")
			{
				console.log("E' un device MASTER, comunico a tutti la scelta della experience");
				broker.publish('PUBLISHER_SET_EXPERIENCE',experience);
			}
			else
			{
				console.log("E' un device SLAVE, non faccio niente");
			}


			/*
			//per test dopo 5 secondo cambio colore
			var updateObj=new Object();
			updateObj.deviceId=id;
			updateObj.type="CHANGE_VALUE";
			updateObj.controlId="SENSOR_1";
			updateObj.value="-1";

			setTimeout( function() {
				broker.publish('PUBLISHER_EVENT',updateObj);
			  }, 5000 );

			//ogni 6 secondi mando un messaggio


			setInterval( function() {
				var updateObj=new Object();
				updateObj.deviceId="TTM_1";
				updateObj.type="CHANGE_VALUE";
				updateObj.controlId="TEXTLIST_1";
				
				var data = new Date();
				var hh = data.getHours();
				var min = data.getMinutes();
				var ss = data.getSeconds();
				var messaggio = "Ora del server: " + (hh>9 ? '' : '0') + hh + ":" + (min>9 ? '' : '0') + min + ":" +(ss>9 ? '' : '0') + ss;
				updateObj.value=messaggio;
				broker.publish('PUBLISHER_EVENT',updateObj);
			}, 6000 );
*/


		
		});		

	}		


	 var _createExperienceList = (id) => {
		
		let _root_config_folder = path.join(__dirname, '..', '..', "configFiles");
		let _fileExperience = path.join(_root_config_folder, 'experiences.json');

		fs.readFile(_fileExperience, 'utf8', (err, data) => {
			var experienceObj=new Object();
			experienceObj.experience=new Array();
			if (err == null)
			{
				try {
					var dataObj=JSON.parse(data);
				}
				catch(err2) {
					console.log("Errore nel parse del file di experiences per il device: "+id);
				}
				//creo l'oggetto da spedire
				for (var i=0;i<dataObj.Experiences.length;i++)
				{
					//vedo se questa experience è per il device in oggetto
					if (dataObj.Experiences[i].device.indexOf(id)!=-1) //esiste
					{
						var temp=new Object();
						temp.name = dataObj.Experiences[i].name;
						temp.id = dataObj.Experiences[i].id;
						temp.image = dataObj.Experiences[i].image;
						experienceObj.experience.push(temp);
					}
				}

			}
			else
			{

			}
			
			experienceObj.deviceId=id;
			broker.publish('PUBLISHER_EXPERIENCE_REPLY',experienceObj);

		
		});		

	}		





	return {
		createConfig:_createConfig,
		createExperienceList:_createExperienceList
	}


})();