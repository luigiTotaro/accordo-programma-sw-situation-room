'use strict';
const config = require('../../config.js');
const fs = require('fs');
const broker = require('../broker/broker.js');
const path = require('path');


module.exports = (() => {

	let testIndexFile=0;


	 var _messageArrived = (oggetto, isTest) => {
		
		let _root_test_folder=null;
		_root_test_folder = path.join(__dirname, '..', '..', "testFolder");
		
		if (testIndexFile>10) return;

		if (!isTest) //situazione normale, a regime arriveranno i messaggi da tradurre
		{

		}
		else //vado con i messaggi di test
		{

			let _fileTest = path.join(_root_test_folder, 'grafoData_'+testIndexFile+'.json');

			fs.readFile(_fileTest, 'utf8', (err, data) => {
				var grafoObj=new Object();
				if (err == null)
				{
					try {
						grafoObj=JSON.parse(data);
					}
					catch(err2) {
						console.log("Errore nel parse del file per il grafoData_"+testIndexFile+".json");
						grafoObj=null;
					}
				}
				else
				{
					console.log("Errore "+err+" nel caricamento del file per il grafoData_"+testIndexFile+".json");
					grafoObj=null;
				}

				testIndexFile++;
				
				if (grafoObj!=null)
				{
					//lo aggiungo alla mia lista stored
					var trovato=false;
					for (var i=0;i<global.storedData.length;i++)
					{
						if (global.storedData[i].target==grafoObj.target)
						{
							trovato = true;
							global.storedData[i].data.push(grafoObj);
						}
					}
					if (!trovato)
					{
						var tempObj= new Object();
						tempObj.target=grafoObj.target;
						tempObj.data= new Array();
						tempObj.data.push(grafoObj);
						global.storedData.push(tempObj);
					}
					broker.publish('PUBLISHER_DATA',global.storedData);

				
				}
				
			
			});		
	


		}
		

	}		


	 var _createExperienceList = (id) => {
		
		let _root_config_folder = path.join(__dirname, '..', '..', "configFiles");
		let _fileExperience = path.join(_root_config_folder, 'experiences.json');

		fs.readFile(_fileExperience, 'utf8', (err, data) => {
			var experienceObj=new Object();
			experienceObj.experience=new Array();
			if (err == null)
			{
				try {
					var dataObj=JSON.parse(data);
				}
				catch(err2) {
					console.log("Errore nel parse del file di experiences per il device: "+id);
				}
				//creo l'oggetto da spedire
				for (var i=0;i<dataObj.Experiences.length;i++)
				{
					//vedo se questa experience è per il device in oggetto
					if (dataObj.Experiences[i].device.indexOf(id)!=-1) //esiste
					{
						var temp=new Object();
						temp.name = dataObj.Experiences[i].name;
						temp.id = dataObj.Experiences[i].id;
						temp.image = dataObj.Experiences[i].image;
						experienceObj.experience.push(temp);
					}
				}

			}
			else
			{

			}
			
			experienceObj.deviceId=id;
			broker.publish('PUBLISHER_EXPERIENCE_REPLY',experienceObj);

		
		});		

	}		





	return {
		messageArrived:_messageArrived,
		createExperienceList:_createExperienceList
	}


})();