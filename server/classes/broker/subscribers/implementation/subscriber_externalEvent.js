"use strict";
const iSubscriber = require('../iSubscriber.js');
const broker = require('../../broker.js');


class subscriber_externalEvent extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/external_event'
		});
		console.log('subscriber_externalEvent event');
	}

	on_message(topic, message) {
		console.log('subscriber_externalEvent event', topic, message.toString());
		var messaggio=JSON.parse(message.toString())
		if (messaggio.type=="sensore")
		{
			var updateObj=new Object();
			updateObj.deviceId="TTM_1";
			updateObj.type="CHANGE_VALUE";
			updateObj.controlId="SENSOR_1";
			updateObj.value=messaggio.value;
			broker.publish('PUBLISHER_EVENT',updateObj);
		}
		else if (messaggio.type=="messaggio")
		{
			var data = new Date();
			var mm = data.getMonth() + 1; // getMonth() is zero-based
			var dd = data.getDate();
		  
			var hh = data.getHours();
			var min = data.getMinutes();
			var ss = data.getSeconds();
			var date2Send = data.getFullYear() + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd + " " + (hh>9 ? '' : '0') + hh + ":" + (min>9 ? '' : '0') + min + ":" +(ss>9 ? '' : '0') + ss;

			
			
			var updateObj=new Object();
			updateObj.deviceId="TTM_1";
			updateObj.type="CHANGE_VALUE";
			updateObj.controlId="TEXTLIST_1";
			updateObj.msgType=messaggio.msgType;
			updateObj.msgFrom=messaggio.msgFrom;
			updateObj.msgTime=date2Send;
			updateObj.msgText=messaggio.value;
			updateObj.priority=messaggio.priority;
			broker.publish('PUBLISHER_EVENT',updateObj);
		}
		if (messaggio.type=="barra")
		{
			var updateObj=new Object();
			updateObj.deviceId="TTM_1";
			updateObj.type="CHANGE_VALUE";
			updateObj.controlId="BAR_1";
			updateObj.value=messaggio.value;
			updateObj.valueMin=messaggio.min;
			updateObj.valueMax=messaggio.max;
			updateObj.priority=messaggio.priority;
			broker.publish('PUBLISHER_EVENT',updateObj);
		}
		if (messaggio.type=="graficoVini")
		{
			var updateObj=new Object();
			updateObj.deviceId="TTM_1";
			updateObj.type="CHANGE_VALUE_CONTENT";
			updateObj.controlId="CHARTWINE_1";
			updateObj.item=messaggio.item;
			updateObj.value=messaggio.value;
			broker.publish('PUBLISHER_EVENT',updateObj);
		}

	}

}

module.exports = subscriber_externalEvent;