"use strict";
const iSubscriber = require('../iSubscriber.js');
const sharedStatusManager = require('../../../helpers/sharedStatusManager.js');


class subscriber_request_data extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/request_data'
		});
		console.log('subscriber_request_data event');
	}

	on_message(topic, message) {
		console.log('subscriber_request_data event', topic, message.toString());
		var messaggio=message.toString()
		sharedStatusManager.sendData4Id(messaggio);
	}

}

module.exports = subscriber_request_data;