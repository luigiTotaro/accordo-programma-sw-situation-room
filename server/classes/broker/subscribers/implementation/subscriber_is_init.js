"use strict";

const iSubscriber = require('../iSubscriber.js');
const sharedStatusManager = require('../../../helpers/sharedStatusManager.js');

class subscriber_is_init extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/is_init'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_is_init event', topic, message.toString());
		var messaggio=JSON.parse(message.toString())
		sharedStatusManager.setDeviceInit(messaggio);
	}

}

module.exports = subscriber_is_init;