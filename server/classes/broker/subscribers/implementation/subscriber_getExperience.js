"use strict";
const iSubscriber = require('../iSubscriber.js');
const configManager = require('../../../helpers/configManager.js');


class subscriber_getExperience extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/experience'
		});
		console.log('subscriber_getExperience event');
	}

	on_message(topic, message) {
		console.log('subscriber_getExperience event', topic, message.toString());
		var messaggio=JSON.parse(message.toString())
		configManager.createExperienceList(messaggio.deviceId);
	}

}

module.exports = subscriber_getExperience;