"use strict";
const iSubscriber = require('../iSubscriber.js');
const configManager = require('../../../helpers/configManager.js');


class subscriber_getConfig extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/config'
		});
		console.log('subscriber_getConfig event');
	}

	on_message(topic, message) {
		console.log('subscriber_getConfig event', topic, message.toString());
		var messaggio=JSON.parse(message.toString())
		configManager.createConfig(messaggio.deviceId, messaggio.experience);
	}

}

module.exports = subscriber_getConfig;