"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_config_reply extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_CONFIG_REPLY',
	    	topic: '/config_reply'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_CONFIG_REPLY Invio messaggio.....");
		super.publish(client, message);
	
	}
	

}

module.exports = publisher_config_reply;