"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_experience_reply extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_EXPERIENCE_REPLY',
	    	topic: '/experience_reply'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_EXPERIENCE_REPLY Invio messaggio.....");
		super.publish(client, message);
	
	}
	

}

module.exports = publisher_experience_reply;