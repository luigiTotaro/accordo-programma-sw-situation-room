"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_set_experience extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_SET_EXPERIENCE',
	    	topic: '/set_experience'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_SET_EXPERIENCE Invio messaggio.....");
		super.publish(client, message);
	
	}
	

}

module.exports = publisher_set_experience;