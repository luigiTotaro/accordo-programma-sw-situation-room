"use strict";
const iPublisher = require('../iPublisher.js');


class publisher_shared_status extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_SHARED_STATUS',
	    	topic: '/sharedStatus'
	    });
	}

	publish(client, message) {
		
		console.log("PUBLISHER_SHARED_STATUS Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_shared_status;