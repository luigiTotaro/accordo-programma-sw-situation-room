"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_data extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_DATA',
	    	topic: '/data'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_DATA Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);		
	}
	

}

module.exports = publisher_data;