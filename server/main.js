'use strict';
const broker = require('./classes/broker/broker.js');
const db_conn = require('./classes/database/db_connection.js');
const config = require('./config.js');
const logger = require('./classes/utils/logger.js');
const http = require("http");
//const customHttpClient = require('./classes/utils/CustomHttpClient.js');
//var CryptoJS = require("crypto-js");
const restInterface = require('./classes/services/restInterface.js');
const scheduler = require('./classes/schedule/scheduler.js');
const sharedStatusManager = require('./classes/helpers/sharedStatusManager.js');
const translatorManager = require('./classes/helpers/translatorManager.js');
//const commandBuilder = require('./classes/CommandBuilder/CommandBuilder.js');

/**/
//logger.log('debug', 'owijsiwosjiow');

/**
 * main module. entry point per il servizio agent
 * avviare con: node main.js 
 * @module main
 */

global.firstStart = true;
global.onlineDevice = new Array();
global.storedData=new Array;


//test
global.smsSent=200;

brokerConnect();

function brokerConnect()
{
	
	logger.log('info','Connessione al Broker - Broker IP: '+config.broker_srv_opts.host+ " - Broker Port: " + config.broker_srv_opts.port + " - Broker Username: " + config.broker_srv_opts.username);
	broker.connect({
		on_connect: () => {
			console.log("Connesso");

				init(); 


		},
		on_offline: () => {
			console.log("Offline");

		}
	});

}


function init()
{

	setInterval( function() {
		sharedStatusManager.checkDeviceAlive();
	}, 10000 );

	setInterval( function() {
		translatorManager.messageArrived(null, true);
	}, 2000 );


/*
	//mando ogni tot i dati finanziari
	setInterval(function()
	{ 
		var nasdaq=8354.29;
		var dowjones= 27881.72;
		var ftsemib=23115.29;
		var nasdaqVar="";
		var dowjonesVar="";
		var ftsemibVar="";

		var rnd=((Math.random()*10)-5);
		if (rnd<0) nasdaqVar="<span style='margin-left:5px;color:red;'>&#8595;</span>";
		else nasdaqVar="<span style='margin-left:5px;color:green;'>&#8593;</span>";
		nasdaq=nasdaq+rnd;
		
		rnd=((Math.random()*10)-5);
		if (rnd<0) dowjonesVar="<span style='margin-left:5px;color:red;'>&#8595;</span>";
		else dowjonesVar="<span style='margin-left:5px;color:green;'>&#8593;</span>";
		dowjones=dowjones+rnd;
		
		rnd=((Math.random()*10)-5);
		if (rnd<0) ftsemibVar="<span style='margin-left:5px;color:red;'>&#8595;</span>";
		else ftsemibVar="<span style='margin-left:5px;color:green;'>&#8593;</span>";
		ftsemib=ftsemib+rnd;

		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="NASDAQ_VALUE";
		updateObj.value="<span>" + nasdaq.toFixed(2) + nasdaqVar;
		broker.publish('PUBLISHER_EVENT',updateObj);		
		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="DOWJONES_VALUE";
		updateObj.value="<span>" + dowjones.toFixed(2) + dowjonesVar;
		broker.publish('PUBLISHER_EVENT',updateObj);		
		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="FTSEMIB_VALUE";
		updateObj.value="<span>" + ftsemib.toFixed(2) + ftsemibVar;
		broker.publish('PUBLISHER_EVENT',updateObj);		

	}, 
	9000);
	
	//mando ogni tot i dati finanziari
	setInterval(function()
	{ 
		var data = new Date();
		var mm = data.getMonth() + 1; // getMonth() is zero-based
		var dd = data.getDate();
		var hh = data.getHours();
		var min = data.getMinutes();
		var ss = data.getSeconds();
		var date2Send = data.getFullYear() + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd + " " + (hh>9 ? '' : '0') + hh + ":" + (min>9 ? '' : '0') + min + ":" +(ss>9 ? '' : '0') + ss;
		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="TEXTLIST_1";
		updateObj.msgTime=date2Send;
		updateObj.priority=false;

		var rnd=(Math.floor(Math.random()*3));
		if (rnd==0)
		{
			updateObj.msgType="INFO";
			updateObj.msgFrom="Firewall di Sistema";
			updateObj.msgText="Firewall attivo e online";
		}
		else if (rnd==1)
		{
			updateObj.msgType="WARNING";
			updateObj.msgFrom="Sistema di controllo accessi";
			updateObj.msgText="Tentativo di accesso al sistema";
		}
		else
		{
			updateObj.msgType="DANGER";
			updateObj.msgFrom="Controllo Intrusioni";
			updateObj.msgText="Intrusione segnalata verso il sistema X";
		}

		broker.publish('PUBLISHER_EVENT',updateObj);
		
		

	}, 
	5000);
	
	setInterval(function()
	{ 	
		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="BAR_1";
		updateObj.value=global.smsSent + (Math.floor(Math.random()*1000));
		updateObj.valueMin=0;
		updateObj.valueMax=10000;
		updateObj.priority=false;
		broker.publish('PUBLISHER_EVENT',updateObj);		
		global.smsSent = updateObj.value;
		if (global.smsSent>10000) global.smsSent=1000;

		
		var updateObj=new Object();
		updateObj.deviceId="TTM_1";
		updateObj.type="CHANGE_VALUE";
		updateObj.controlId="SENSOR_1";
		var rnd=(Math.floor(Math.random()*2));
		if (rnd==0)
		{
			updateObj.value=-1;
		}
		else
		{
			updateObj.value=1;
		}

		
		broker.publish('PUBLISHER_EVENT',updateObj);

	}, 
	2000);	

*/


	//console.log(JSON.stringify(result));
	//broker.publish('PUBLISHER_EVENT',result);

	


/*
	//poi sarà fatto dall'onconnect del broker
	agentService.initialize((err) => {
        	
		if (err) {
			logger.log('error',err);
			process.exit();
		} else {
			if (!config.testMode)
			{
				//esecuzione standard
				scheduler.run("scheduler_devicestatus");
				scheduler.run("scheduler_events");
				scheduler.run("scheduler_synctime");
				scheduler.run("scheduler_isAlive");
			}
			else
			{
				
				



			}

			
			

		}

	}); 
*/
}
	



