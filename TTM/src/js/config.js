var path = require('path');
var fs = require('fs');

 
/**
 * modulo di configurazione contiene i parametri di inizializzazione di tutta l'applicazione.
 * Questo modulo deve accentrare tutte le opzioni di configurazione
 * @module config
 * @param {json} broker_srv_opts - impostazione per la connessione al broker mqtt
 * @param {string} broker_srv_opts.port - porta di connessione al broker mqtt
 * @param {string} broker_srv_opts.host - indirizzo di connessione al broker mqtt
 * @param {string} broker_srv_opts.clean - true/false indica se si vuole eseguire una connessione 'clean' verso il broker MQTT {@link https://www.hivemq.com/docs/hivemq/latest/}
 * @param {string} broker_srv_opts.clientId - id del client verso il broker. è obbligatorio se il parametro clean è 'true'
 * @param {int} broker_srv_opts.protocolVersion - versione del protocollo MQTT (di default è 4)
 * @param {string} broker_srv_opts.key - contenuto della chiave privata in formato .pem del client. il certificato viene emesso dal server e conservato sul server in un trust store
 * @param {string} broker_srv_opts.cert - contenuto del certificato in formato .pem del client. il certificato viene emesso dal server e conservato sul server in un trust store
 * @param {string} broker_srv_opts.rejectUnauthorized - impostare su false in caso di certificati autoprodotti
 * @param {string} broker_srv_opts.protocol - tipo di protocollo 'mqtts','mqtt'. {@link https://www.npmjs.com/package/mqtt#connect} 
 * @param {string} broker_srv_opts.username - nome utente
 * @param {string} broker_srv_opts.password - password
 * @param {int} broker_srv_opts.reconnectPeriod - delta temporale prima di ritentare una connessione
 * @param {int} broker_srv_opts.connectTimeout - timeout di connessione (allo scadere il client genera un evento 'reconnect')
 * @param {json} broker_subscribe_ops - opzioni utilizzate dal client per la sottoscrizione ad un topic
 * @param {int} broker_subscribe_ops.qos - qos da applicare nella sottoscrizione 
 * @param {json} broker_publish_ops - opzioni utilizzate dal client per la pubblicazione verso un topic
 * @param {int} broker_publish_ops.qos - qos da applicare nella sottoscrizione 

 * @param {json} database - opzioni per la connessione verso un database mssql
 * @param {boolean} database.start_local_sql_browser - true/false, indica se far partire il servizio SQL Browser perima di connettersi al db
 * @param {string} database.user - nome utente
 * @param {string} database.password - password
 * @param {string} database.server - indirizzo ip del server
 * @param {string} database.database - nome del database
 * @param {int} database.pool.max - numero massimo di connessioni verso il pool
 * @param {int} database.pool.min - numero minimo di connessioni verso il pool
 * @param {int} database.pool.idleTimeoutMillis - tempo di inattività prima di chiudere una connessione
 * @param {int} database.options.encrypt - crypt della connessione (true se Windows Azure)
 * @param {json} logfile - opzioni per il logging su file secondo le modalità della libreria winston {@link https://github.com/winstonjs/winston}
 * @param {string} logfile.file - nome del file (oppure nome con percorso completo)
 * @param {int} logfile.maxsize - massima dimensione del file di log prima di effettuare una rotazione

 * @param {json} schedulers - oggetto contenente la lista degli scheduler da instanziare a runtime. l'implementazione è mutuata dalle librerie {@link https://www.npmjs.com/package/node-schedule}
 * @param {string} schedulers.<scheduler_id> - identificativo del task di scheduling da instanziare
 * @param {string} schedulers.<scheduler_id>.cron - identificativo del task di scheduling da instanziare
 * @param {string} schedulers.<scheduler_id>.class - nome del file contenente l'implementazione del comando di schedule. tale file deve dichiarare una classe con lo stesso nome del file (es: comando0 nel file ./implementation/comando0.js)
 * @param {boolean} schedulers.<scheduler_id>.autostart - true/false, indica se quello scheduler deve anche partire una volta instanziato oppure deve aspettare una richiesta esplicita
 
  * @param {string} HWProfile - nome del profilo hardware, recuperato tramite accesso al db
  * @param {string} NetworkAdapterName1 - nome della scheda di rete principale del sistema
  * @param {string} NetworkAdapterName2 - nome della scheda di rete secondaria del sistema
  * @param {string} machineIp - IP associato alla scheda di rete principale del sistema
  * @param {string} machineMac - MAC address associato alla scheda di rete principale del sistema
  * @param {string} machineMac2 - MAC address associato alla scheda di rete secondaria del sistema
  * @param {boolean} SendConfigAndStatusAtServiceStart - true/false, indica se deve inviare la configurazione e lo stato del server una volta che l'agent è partito (dafault: true)
  * @param {boolean} SendEventsAtServiceStart - true/false, indica se deve inviare gli eventi del server una volta che l'agent è partito (dafault: true)
  * @param {boolean} SendParametersAtServiceStart - true/false, indica se deve inviare l'update della configurazione del server quando richiesto (dafault: true)
  * @param {int} SendAllStatusMinutes - intervallo di tempo, in minuti. Impone di mandare uno stato completo del server (invece di un delta) se è trascorso queto intervallo dall'ultimo invio di uno stato completo
  * @param {string} MachineExternalIP - IP esterno della macchina. Per adesso usiamo l'ip interno come IP esterno. In futuro lo recupereremo in altro modo

  * @param {json} SyncDataset_config - oggetto contenente informazioni necessarie per poter effettuare il calcolo delle differenze per l'invio delle configurazioni delta
  * @param {string} SyncDataset_config.name - nome della tabella del database
  * @param {string} SyncDataset_config.compareColumn - colonne della tabella da usare per la comparazione. se=*, prende in considerazione tutte le tabelle
  * @param {string} SyncDataset_config.notSentColumn - colonne da non mandare
  * @param {string} SyncDataset_config.pk - Colonna Primary Key della tabella

  * @param {json} SyncDataset_devicestatus - oggetto contenente informazioni necessarie per poter effettuare il calcolo delle differenze per l'invio dello stato dei device delta
  * @param {string} SyncDataset_devicestatus.name - nome della tabella del database
  * @param {string} SyncDataset_devicestatus.compareColumn - colonne della tabella da usare per la comparazione. se=*, prende in considerazione tutte le tabelle
  * @param {string} SyncDataset_devicestatus.notSentColumn - colonne da non mandare
  * @param {string} SyncDataset_devicestatus.pk - Colonna Primary Key della tabella
 */
module.exports = {

	broker_srv_opts: {
		port: '8883',
		host: '192.170.5.81',
		clean:false,
		protocolVersion: 3,
		key: fs.readFileSync(path.join(__dirname, '../certs', 'client-key.pem')),
		cert: fs.readFileSync(path.join(__dirname, '../certs', 'client-cert.pem')),
		rejectUnauthorized: false,
		protocol: 'mqtts',
		clientId: 'client_' + (new Date()).getTime(),
		username: 'hivemq-user3',
		password: 'user3',
		reconnectPeriod: 10000,
		connectTimeout: 20000
	},


	broker_subscribe_ops: {
		qos:2
	},


	broker_publish_ops: {
		qos:2
	},


	database: {
		//SVILUPPO
		
		start_local_sql_browser: false,
		user: 'telefinDev',
		password: 'telefinDev',
	    //server: '192.170.5.21',
	    server: '10.211.55.6',

		database: 'Telefin',

	    options: {
	        encrypt: false 
	    },
	    
		
		// PRODUZIONE
		/*
		start_local_sql_browser: true,
	    user: 'sa',
	    password: 'StLc!000',
	    server: '127.0.0.1',
	    database: 'telefin',

	    options: {
	        encrypt: false,
			instanceName: 'SQLEXPRESS',
			trustedConnection: true,
			localAddress: '127.0.0.1'
	    },
	    */

	    pool: {
	        max: 10,
	        min: 5,
	        idleTimeoutMillis: 10000
	    }

	},


	logfile: {
		file: 'log.txt',
		level: 'info', //debug, info, warn, error
		maxsize: 100*1024*5,
		maxfiles: 1
	},


	schedulers: {
		
		scheduler_devicestatus: {
			cron: '*/5 * * * *', //ogni quinto minuto
			class: 'ScheduleCommandDeviceStatus',
			autostart: false
		},

		scheduler_events: {
			cron: '0 * * * *', //ogni minuto 0 di ogni ora (dunque ogni ora)
			class: 'ScheduleCommandEvents',
			autostart: false
		},

		scheduler_synctime: {
			cron: '0 * * * *', //ogni minuto 0 di ogni ora (dunque ogni ora)
			class: 'ScheduleCommandSyncTime',
			autostart: false
		},

		scheduler_isAlive: {
			cron: '*/1 * * * *', //ogni minuto
			class: 'ScheduleCommandIsAlive',
			autostart: false
		},
		
		//scheduler1: {
		//	cron: '*/5 * * * * *',
		//	class: 'ScheduleCommand1'
		//}

	},

	HWProfile: "STLC",

	NetworkAdapterName1 : "Connessione alla rete locale (LAN)", //"en0",
	
	NetworkAdapterName2 : "en5",

	machineIp : "",

	machineMac : "",

	machineMac2 : "",

	NodID : "", //preso all'avvio dal db, serve per comporre il subscriber per i comandi dal gris centrale

	SendConfigAndStatusAtServiceStart : true,

	SendEventsAtServiceStart : true,

	SendParametersAtServiceStart : true,

	SendConfigChange : true,

	SendAllStatusMinutes: 720,

	SendIsAliveMinutes: 0,

	MachineExternalIP : "",

	testMode : true,

	overrideBrokerConnection : false, //default false, se =true, non prova la connessione al broker e parte direttamente

	sendMessagesToRestService : false,

	restServiceAddress: "localhost",

	restServicePort: 8916,

	getBrokerInfoFromServer : false, //se =true, interroga il server con il region id e recupera le info per la connessione al broker. Se = false, usa una connessione diretta

	brokerInfoAddress : "http://localhost:50923/GrisSuite.GRIS/agentService.ashx", //indirizzo per recuperare le info sul broker di compartimento
	
	SyncDataset_config : [
		{
			name: "station",
			compareColumn: "*",
			notSentColumn: "",
			pk: "StationID"
		},
		{
			name: "building",
			compareColumn: "*",
			notSentColumn: "",
			pk: "BuildingID"
		},
		{
			name: "rack",
			compareColumn: "*",
			notSentColumn: "",
			pk: "RackID"
		},
		{
			name: "servers",
			compareColumn: "*",
			notSentColumn: "",
			pk: "SrvID"
		},
		{
			name: "port",
			compareColumn: "*",
			notSentColumn: "",
			pk: "PortID"
		},
		{
			name: "devices",
			compareColumn: "*",
			notSentColumn: "",
			pk: "DevID"
		}
	],

	SyncDataset_devicestatus : [
		{
			name: "devicestatus",
			compareColumn: "SevLevel,Offline,Description,ShouldSendNotificationByEmail",
			notSentColumn: "",
			pk: "DevID"
		},
		{
			name: "streams",
			compareColumn: "Visible,SevLevel",
			notSentColumn: "",
			pk: "DevID,StrID"
		},
		{
			name: "streamfields",
			compareColumn: "DevID,SevLevel,ReferenceID,Description,ShouldSendNotificationByEmail",
			notSentColumn: "",
			pk: "FieldID,ArrayID,StrID,DevID"
		},
		{
			name: "reference",
			compareColumn: "*",
			notSentColumn: "",
			pk: "ReferenceID"
		}
	]



}