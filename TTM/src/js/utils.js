
global.listaUtenti = [];
global.listaStanze = [];
global.listaUtentiById = [];

global.restURL = "http://192.170.5.46:8080";
var $ = require('jquery');



var Utils = function() {
};


Utils.getListaUtenti = function() {

    // chiamata ajax
    $.ajax({
          url: global.restURL+"/devicesmobili/",
          method: 'GET',

          success: function(events)
          {

            for (var event in events)
            {
            	console.log(events[event]);
                	
            	//creo una entry per ogni item
  				    if (events[event].mac_address!="")
  				    {
    					    var newMac= events[event].mac_address.replace(/:/gi, "_"); 
    					    global.listaUtenti[newMac] =
                  {
    					    	id: events[event].id,
    					    	nome: events[event].nome,
    					    	numero_matricola: events[event].numero_matricola,
    					    	idUtente: events[event].utente.id,
    					    	nomeUtente: events[event].utente.nome,
    					    	tipo: events[event].utente.tipo
    					    }
                  global.listaUtentiById[events[event].utente.id] =
                  {
                    nome: events[event].nome,
                    numero_matricola: events[event].numero_matricola,
                    idUtente: events[event].utente.id,
                    nomeUtente: events[event].utente.nome,
                    tipo: events[event].utente.tipo,
                    immagine: global.restURL+"/photo/guest/photo_"+events[event].utente.id+".jpg"
                  }

    					}
            }
            //ho preso gli utenti, ora prendo il mapping tra stanze e celle
            Utils.getMappingStanze();

            console.log(global.listaUtentiById);

          },

          failure: function(jqx, err) {
                console.log("Errore: " + err);
          }

    });





}

Utils.getMappingStanze = function() {

    // chiamata ajax
    $.ajax({
          url: global.restURL+"/cellslocations/7",
          method: 'GET',

          success: function(events)
          {
            //console.log(events);
            for (var event in events)
            {
              //console.log(events[event]);
                  
              //creo una entry per ogni item
              global.listaStanze[events[event].codice.toUpperCase()] =
              {
                nome: events[event].nome
              }
            }
            console.log("lista stanze: ")
            console.log(global.listaStanze);
            

          },

          failure: function(jqx, err) {
                console.log("Errore: " + err);
          }

    });





}



Utils.getUtenteByMac = function(mac) {

    var newMac= mac.replace(/:/gi, "_"); 


    return global.listaUtenti[newMac];

}

Utils.getUtenteById = function(id) {

    return global.listaUtentiById[id];

}

Utils.getStanzaByCell = function(cell) {

    return global.listaStanze[cell].nome;

}



module.exports = Utils;