//const config = require('../../config.js');


module.exports = (() => {

var mainDom=null;

	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _disegna = (layout) => {
		//mi devo creare un array di oggetti da disegnare con il parent di ognuno
		var oggetto2render= new Array();
		for (var i=0;i<layout.length;i++)
		{
			var inner=new Object();
			inner.element=layout[i];
			inner.idDom=null;
			oggetto2render.push(inner);
			//ci sono elementi interni?
			var elementi=layout[i].elements;
			if (typeof(elementi)!=undefined)
			{
				for (var t=0;t<elementi.length;t++)
				{
					var inner=new Object();
					inner.element=elementi[t];
					inner.idDom=layout[i].id;
					oggetto2render.push(inner);
				}
			}
		}
		console.log(oggetto2render);
		global.oggetti2render=oggetto2render;
		for (var i=0;i<oggetto2render.length;i++)
		{
			_render(oggetto2render[i]);
		}		


	}



	var _render = (oggetto) => {

		var dom=null;
		if (oggetto.idDom==null) dom=mainDom;
		else dom=mainDom.find("#"+oggetto.idDom+"");
		var layout=oggetto.element;
		console.log("Sono dentro _disegna, con parent: " + dom.attr("id"));
		console.log(layout);

		var html="";
		switch(layout.type) {
			case "container":
				html="<div id='"+layout.id+"' style='position:absolute;top:"+layout.posY+";left:"+layout.posX+";width:"+layout.width+";height:"+layout.height+";"+layout.style+"'></div>";
				//console.log(html);
				dom.append(html);
				console.log("ho disegnato un container, dom: " + dom.attr("id"));

			break;
			case "led":
				var stati=layout.states;
				var initialState=stati[layout.initialState];
				html="<div id='"+layout.id+"' style='position:absolute;top:"+layout.posY+";left:"+layout.posX+";'><div name='led' style='border:1px solid #000000;width:"+layout.width+";height:"+layout.width+";border-radius:"+layout.width+";"+initialState+";float:left'></div><p style='margin: 0px;line-height: "+layout.width+";float: left;margin-left: 10px;'>"+layout.label+"</p></div>";
				//console.log(html);
				dom.append(html);
			break;
			case "textList":
				html="<div id='"+layout.id+"' style='position:absolute;top:"+layout.posY+";left:"+layout.posX+";width:"+layout.width+";height:"+layout.height+";border:1px solid #000000;overflow:auto;'><p style='margin: 10px;'>"+layout.label+"</p><div name='testo' style='padding:10px;'></div></div>";
				//console.log(html);
				dom.append(html);

			break;
			default:
				// code block
		}

	}		

	var _aggiorna = (oggetto) => {

		//questo oggetto da aggiornare, cosa è?

		for (var i=0;i<global.oggetti2render.length;i++)
		{
			if (global.oggetti2render[i].element.id!=oggetto.controlId) continue;

			switch(global.oggetti2render[i].element.type) {
				case "led":
					var stati=global.oggetti2render[i].element.states;
					var actualState=stati[oggetto.value];
					//divido sul :
					var separated = actualState.split(":");
					//trovo quel controllo
					mainDom.find("#"+oggetto.controlId+"").children( "[name='led']" ).css(separated[0],separated[1]);
				break;
				case "textList":
					//devo aggiungere una riga di testo
					mainDom.find("#"+oggetto.controlId+"").children( "[name='testo']" ).append(oggetto.value+"<br>");

				break;
				default:
					// code block
			}
		}

	}	




	var _inserisciContenuti = (contents) => {

		var dom=mainDom;

		for (var i=0;i<contents.length;i++)
		{

			var html="";
			switch(contents[i].type) {
				case "video":
					html="<video id='"+contents[i].id+"' class='draggable' controls style='position:absolute;top:"+contents[i].posY+";left:"+contents[i].posX+";border:1px solid #000000;width:"+contents[i].width+";height:"+contents[i].height+";'>";
					html+="<source src='"+contents[i].src+"' type='video/mp4'/>";
					html+="</div>";
					//console.log(html);
					dom.append(html);
					console.log("ho disegnato un video");


					//html='<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="position:absolute;top:300px;left:10px;border:1px solid #000000;width:200px;height:200px;"';
					//html+='src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Sonzini,+25,+Galatina,+LE&amp;aq=0&amp;oq=via+sonzini+25&amp;sll=41.008099,16.727239&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Sonzini,+25,+Galatina,+Lecce,+Puglia&amp;z=14&amp;ll=40.17726,18.169935&amp;output=embed&iwloc=near">';
					//html+='</iframe>';
					//dom.append(html);

				break;
				case "mappa":
					html='<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="position:absolute;top:'+contents[i].posY+';left:'+contents[i].posX+';border:1px solid #000000;width:'+contents[i].width+';height:'+contents[i].height+';"';
					html+='src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;ll='+contents[i].center+'&amp;t=m&amp;ie=UTF8&amp;hq=&amp;z=15&amp;output=embed&iwloc=near">';
					html+='</iframe>';
					dom.append(html);				
				break;
				case "":

				break;
				default:
					// code block
			}
		}
	}	



	/** 
 	* @function '_bufferReviver' - opzione per JSON.parse, che permette di ricreare correttamente un oggetto con all'interno dei Buffer
 	* @param {object} oggetto - oggetto dal quale recuperare il numero di righe
	* @returns {int} numero di righe
	 */
	var _setDom = (dom) => {  
		mainDom = dom;
	};  



	return {
		disegna:_disegna,
		aggiorna:_aggiorna,
		inserisciContenuti:_inserisciContenuti,
		setDom:_setDom
	}


})();