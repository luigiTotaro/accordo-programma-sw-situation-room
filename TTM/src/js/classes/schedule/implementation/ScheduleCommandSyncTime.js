"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const agentService = require('../../services/agentService.js');
const logger = require('../../utils/logger.js');


class ScheduleCommandSyncTime extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/gamma/delta'
	    });
	}

	run() {
		logger.log('info', '--- Scheduler sync Time');
		
		agentService.syncTime((err) => {
			if (err)
			{
				logger.log('error',err);
			}
		});
			
	}

}

module.exports = ScheduleCommandSyncTime;