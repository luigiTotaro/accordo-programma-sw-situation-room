"use strict";
const config = require('../../config.js');
const hwInfo = require('../utils/hwInfo.js');
const CommonDAO = require('../database/CommonDAO.js');
const AgentDAO = require('../database/AgentDAO.js');
const Agent = require('../helpers/agent.js');
const logger = require('../utils/logger.js');

let _isDeviceStatusSending=false;
let _isEventsSending=false;

module.exports = (() => {

	/** 
 	* @function 'initialize' - Questa funzione chiama l'initializa della classe Agent, che recupera il Server ID dal db
 	* @param {callback} on_complete - function(err)
 	*/
	 var _initialize = (on_complete) => {

		console.log("Agentservice initialize");
		
		Agent.initialize((err) => {
        	
			global.lastMessageSent = Date.now(); //se passano più di 5 minuti dall'ultimo messaggio inviato, invio un isAlive
			//per adesso usiamo l'ip interno come IP esterno. In futuro lo recupereremo in altro modo
			if (config.MachineExternalIP == "") config.MachineExternalIP = hwInfo.getIP(config.NetworkAdapterName1);
			on_complete(err);	
		
		}); 

	}

	/** 
 	* @function 'sendDeviceStatusToCentral' - Esegue operazioni di aggiornamento e manda lo status della macchina (completo o differenziale al server centrale)
 	* @param {callback} on_complete - function(err)
 	*/
	var _sendDeviceStatusToCentral = (on_complete) => {

		logger.log('info','AgentService: sendDeviceStatusToCentral');
		if (_isDeviceStatusSending)
		{
			logger.log('info','AgentService: sendDeviceStatusToCentral already running.... wait for the next timer tick');
			on_complete(null);
			return;
		}
		
		_isDeviceStatusSending=true;
		_updateServerFromEnvironment((err) => {
			
			if (err)
			{
				_isDeviceStatusSending=false;
				on_complete(err)
			}
			else
			{
				if (global.firstStart)
				{
					_sendStatusComplete();
				}
				else
				{
					_sendStatusUpdate();
				}			
			}
		});
	}

	/** 
 	* @function 'sendEventsToCentral' - Manda gli eventi della macchina al server centrale
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendEventsToCentral = (on_complete) => {

		if (_isEventsSending)
		{
			logger.log('info','AgentService: sendEventsToCentral already running.... wait for the next timer tick');
			on_complete(null);
			return;
		}

		_isEventsSending=true;
		//logger.log('info','AgentService: sendEventsToCentral');
		Agent.sendEvents((err) => {
			
			_isEventsSending=false;
			on_complete(err)

		});
	}

	/** 
 	* @function 'syncTime' - Manda un messaggio al server centrale per richiedere l'invio del tempo corrente, per sincronizzarsi
 	* @param {callback} on_complete - function(err)
 	*/
	 var _syncTime = (on_complete) => {

		//logger.log('info','syncTime');
		Agent.syncronizeTime((err) => {
			
			on_complete(err)

		});
	}

	
	/** 
 	* @function '_eval_SendStatus_SendConfigAndStatusAtServiceStart' - Se previsto dal file di configurazione, chiama il Reset del config e dello status
 	*/
	var _eval_SendStatus_SendConfigAndStatusAtServiceStart = () => {

		return new Promise(function(resolve, reject) {

			if(config.SendConfigAndStatusAtServiceStart) {
				//logger.log('info','Calling ResetConfigAndStatus()');
				_resetConfigAndStatus((result) => {
					console.log("finito ResetConfigAndStatus: "+result);
					resolve();
				});
			} else {
				console.log("!if SendConfigAndStatusAtServiceStart")
				resolve();
			}
		});
	}


	/** 
 	* @function '_eval_SendStatus_SendEventsAtServiceStart' - Se previsto dal file di configurazione, manda gli eventi al server centrale
 	*/
	var _eval_SendStatus_SendEventsAtServiceStart = () => {
		
		return new Promise(function(resolve, reject) {

			console.log("_eval_SendStatus_SendEventsAtServiceStart");

			if (config.SendEventsAtServiceStart)
			{
				console.log("Calling SendEvents()");
				Agent.sendEvents((result) => {
					console.log("finito SendEvents: "+result);
					resolve();
				});
			} else {
				console.log("!if SendEventsAtServiceStart")
				resolve();
			}
		});
	}


	/** 
 	* @function '_eval_SendStatus_SendParametersAtServiceStart' - Se previsto dal file di configurazione, manda i parametri STLC al server centrale
 	*/
	var _eval_SendStatus_SendParametersAtServiceStart = () => {
		
		return new Promise(function(resolve, reject) {

			console.log("_eval_SendStatus_SendParametersAtServiceStart");

			if (config.SendParametersAtServiceStart)
			{
				console.log("Calling sendSTLCParameters()");
				Agent.sendSTLCParameters((result) => {
					console.log("finito sendSTLCParameters: "+result);
					resolve();
				});
			} else {
				console.log("!if sendSTLCParameters")
				resolve();
			}
		});
	}

	/** 
 	* @function 'sendStatusComplete' - manda lo status completo della macchina al server centrale
 	*/
	var _sendStatusComplete = () => {

		logger.log('info','AgentService: sendStatusComplete');
		let self = this;

		_eval_SendStatus_SendConfigAndStatusAtServiceStart().then(function(value) {
			
			_eval_SendStatus_SendEventsAtServiceStart().then(function(value) {
				
				_eval_SendStatus_SendParametersAtServiceStart().then(function(value) {
				
					console.log("finito promise - sendStatusComplete");
					global.lastStatusCompleteSending = Date.now();
					global.firstStart = false;
					_isDeviceStatusSending=false;
				});				
			
			});
		
		});

	}


	/** 
 	* @function '_eval_SendStatusUpdate_SendConfig' - Se previsto dal file di configurazione, manda gli aggiornamenti della configurazione al server centrale
 	*/	
	var _eval_SendStatusUpdate_SendConfig = () => {
		
		return new Promise(function(resolve, reject) {

			console.log("_eval_SendStatusUpdate_SendConfig");

			if (config.SendConfigChange)
			{
				console.log("Calling sendConfigToCentral()");
				Agent.sendConfigToCentral(true, (result) => { //con true mando la configurazione delta
					console.log("finito sendConfigToCentral: "+result);
					resolve();
				});
			} else {
				console.log("!if SendConfig")
				resolve();
			}
		});
	}

	
	/** 
 	* @function 'sendStatusUpdate' - manda lo status delta della macchina al server centrale
 	*/
	 var _sendStatusUpdate = () => {

		logger.log('info','AgentService: sendStatusUpdate');
		let self = this;

		_eval_SendStatusUpdate_SendConfig().then(function(value) {

			Agent.sendAcks((err) => {

				if (err) logger.log('warn',err);

				//se sono passati più di xxx minuti dall'ultimo invio completo, faccio un invio completo, altrimenti delta
				var adesso = Date.now();
				var target = global.lastStatusCompleteSending + config.SendAllStatusMinutes*60000;
				var only_changes=true;
				if (adesso > target)
				{
					logger.log('info','AgentService: sono passati più di '+config.SendAllStatusMinutes+' minuti dall\'ultimo invio completo, lo rimando completo');
					only_changes=false;
					global.lastStatusCompleteSending = Date.now();
				}
				else
				{
					logger.log('info','AgentService: sono passati meno di '+config.SendAllStatusMinutes+' minuti dall\'ultimo invio completo, mando solo le differenze');
				}
				
				Agent.sendDeviceStatusToCentral(only_changes, (err) => {
					
					if (err) logger.log('warn',err);
					console.log("finito - sendStatusUpdate");
					_isDeviceStatusSending=false;

					
				});


			});


		});

	}



	/** 
 	* @function 'resetConfigAndStatus' - esegue una serie di operazioni per resettare configurazione e status del sistema
 	* @param {callback} on_complete - function(err)
 	*/
	var _resetConfigAndStatus = (on_complete) => {
		logger.log('info','AgentService: ResetConfigAndStatus()');	
		
		Agent.clearCache((err) => {
			//on_complete(result);
			Agent.sendConfigToCentral(false, (err) => { //con false mando la configurazione completa
				if (err) logger.log('warn',err);
				let only_changes=false; 
				Agent.sendDeviceStatusToCentral(only_changes, (err) => {
					if (err) logger.log('warn',err);
					Agent.sendAcks((err) => {
						if (err) logger.log('warn',err);
						on_complete(null);
						//Agent.sendIsAlive((result) => {
						//	on_complete(result);
						//});
					});
				});
			});
		});
	}


	/** 
 	* @function 'updateServerFromEnvironment' - aggiorna le tabelle servers e stlc_parameters in base alla configurazione hw della macchina
 	*/
	var _updateServerFromEnvironment = (on_complete) => {

		logger.log('info','AgentService: updateServerFromEnvironment');
		//recupero alcuni dati che mi servono
		console.log('CPU-Information:');
		config.machineMac=hwInfo.getMac(config.NetworkAdapterName1);
		config.machineMac2=hwInfo.getMac(config.NetworkAdapterName2);
		var fullhostname = hwInfo.getFullHostName();
		config.machineIp = hwInfo.getIP(config.NetworkAdapterName1);
		var HWProfile = config.HWProfile;

		console.log("mac: " + config.machineMac);
		console.log("mac2: " + config.machineMac2);
		console.log("fullhostname: " + fullhostname);
		console.log("ip: " + config.machineIp);
		console.log("HW Profile: " + HWProfile);

		let eval_change_update_servers = function(hasChanged, updateData, on_complete_update_servers) {

			if(hasChanged === false) {
				on_complete_update_servers(null);
			} else {

				AgentDAO.update_servers_table(updateData, (err, result) => {

					if (err)
					{
						on_complete_update_servers(err);
					}
					else
					{
						on_complete_update_servers(null);
					}

				});

			}
		}

		let eval_change_update_stlc_param = function(operationOnSTLCparameter, on_complete_stlc_param) {

			console.log("operationOnSTLCparameter: " + operationOnSTLCparameter);

			if (operationOnSTLCparameter == 0) {
				//niente da fare
				on_complete_stlc_param(null);
			
			} else if (operationOnSTLCparameter == 1) {

				//update
				AgentDAO.update_stlc_parameter(config.machineMac2, 'MACAddress2', (err, result) => {
					if (err) {
						on_complete_stlc_param(err);
					} else {
						on_complete_stlc_param(null);
					}
				});
			
			} else {

				//inserimento
				AgentDAO.insert_stlc_parameter(config.machineMac2, 'MACAddress2', 'MAC address della seconda scheda di rete', (err, result) => {
					if (err) {
						on_complete_stlc_param(err);
					} else {
						on_complete_stlc_param(null);
					}
				});

			}
		}

	    CommonDAO.getServerData(null, (err, result) => {
	        	
			if (err)
			{
				on_complete("Errore nel recupero dati dalla tabella Server")
			}
			else {

				for (var i=0;i<result.length;i++) {

					var hasChanged = false;
					var updateData = new Array();
					//hostname
					if ((result[i].FullHostName==null) || (result[i].FullHostName!=fullhostname)) {
						hasChanged = true;
						updateData.push(`FullHostName='${fullhostname}'`);
					}
					//ip
					if ((result[i].IP==null) || (result[i].IP!=config.machineIp)) {
						hasChanged = true;
						updateData.push(`IP='${config.machineIp}'`);
					}
					//mac
					if ((result[i].MAC==null) || (result[i].MAC!=config.machineMac)) {
						hasChanged = true;
						updateData.push(`MAC='${config.machineMac}'`);
					}

				}
	            
	            eval_change_update_servers(hasChanged, updateData, (err) => {

	            	if(err) {
						logger.log('warn','error during update of server table');
	            	}

	            	//controllo se in stlc_parameters ci sta il mac2
					var resultSTLCparam;					    
				    CommonDAO.getSTLCData((err, resultSTLCparam) => {
				        	
						if (err) {
							logger.log('warn','error during retrieve data from STLC Parameter');
							on_complete(null);
							return;
						} else {

							var operationOnSTLCparameter=null; //se =0 non deve fare niente, se=1 deve fare l'update, se=-1 deve inserire
							for (var i=0;i<resultSTLCparam.stlc_param.length;i++)
							{
								if (resultSTLCparam.stlc_param[i].ParameterName=='MACAddress2')
								{
									if (resultSTLCparam.stlc_param[i].ParameterValue!=config.machineMac2)
									{
										operationOnSTLCparameter=1; //dovrà fare l'update
									}
									else
									{
										operationOnSTLCparameter=0; //è già aggiornato, non deve fare niente
									}
								}
							}
							if (operationOnSTLCparameter==null) //non l'ha proprio trovato
							{
								operationOnSTLCparameter=-1; //devo inserire un nuovo record
							}


				            eval_change_update_stlc_param(operationOnSTLCparameter, (err) => {

				            	if(err) {
				            		logger.log('warn','error during Mac2 update of STLC Parameter table');
				            	}

				            	//riga HWprofile
								let trovato = false;
								let toChange=false;

								for(let i=0; i<resultSTLCparam.stlc_param.length; i++) {

									if(resultSTLCparam.stlc_param[i].ParameterName === 'HardwareProfile') {

										trovato = true;
										
										if(resultSTLCparam.stlc_param[i].ParameterValue !== HWProfile) {

											toChange=true;

										} else {

											toChange=false;	
										}
									}
								}

								if (trovato)
								{
									if (toChange)
									{
										AgentDAO.update_stlc_parameter(HWProfile, 'HardwareProfile', (err, result) => {

											if(err) {
												logger.log('warn','error during HWProfile update of STLC Parameter table');
												on_complete(null);
											} else {
												on_complete(null);
											}

										});
									}
									else
									{
										on_complete(null);
									}
								}
								else
								{
									AgentDAO.insert_stlc_parameter(HWProfile, 'HardwareProfile', 'Profilo hardware del server', (err, result) => {
										if (err) {
											logger.log('warn','error during HWProfile insert in STLC Parameter table');
											on_complete(null);
										} else {
											on_complete(null);
										}
									});
								}


				            });

						}

		            });


	            });

			}

		}); 

	}

	var _testTime = () => {

		Agent.testTime();
	

	}		

	var _isAlive = () => {

		//se sono passati più di xxx minuti dall'ultimo messaggio inviato, mando un is Alive
		var adesso = Date.now();
		var target = global.lastMessageSent + config.SendIsAliveMinutes*60000;
		if (adesso > target)
		{
			logger.log('info','AgentService: sono passati più di '+config.SendIsAliveMinutes+' minuti dall\'ultimo invio di un messaggio, mando un IsAlive');
			Agent.isAlive();
		}
		else
		{
			logger.log('info','AgentService: sono passati meno di '+config.SendIsAliveMinutes+' minuti dall\'ultimo invio di un messaggio, NON mando un isAlive');
		}


	}	

	return {
		sendDeviceStatusToCentral:_sendDeviceStatusToCentral,
		sendEventsToCentral:_sendEventsToCentral,
		syncTime:_syncTime,
		initialize:_initialize,
		testTime:_testTime,
		isAlive:_isAlive
	}


})();