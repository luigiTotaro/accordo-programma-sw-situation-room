"use strict";

const iSubscriber = require('../iSubscriber.js');
const configManager = require('../../../helpers/configManager.js');


class subscriber_config_reply extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/config_reply'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_config_reply event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		configManager.process(messaggio);
	}

}

module.exports = subscriber_config_reply;