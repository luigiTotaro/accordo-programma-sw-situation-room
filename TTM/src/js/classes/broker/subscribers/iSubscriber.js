/**
 * Interfaccia per la gestione delle classi che implementano i subscriber verso il broker. Tutte le 
 * classi che implementano questa interfaccia sono responsabili della sottoscrizione a determinati topic
 * verso il broker
 * @interface iSubscriber
 */

'use strict';

const config = require('../../../config.js');

class iSubscriber {

	constructor(opts) {
		
		this.opts = opts;

		if(!this.opts.topic) {
			throw 'parametro topic obbligatorio';
		}

	}


	get_topic() {
		return this.opts.topic;
	}

	register(client) {

		if(client.constructor.name !== 'MqttClient') {
			throw 'parametro client non di tipo MqttClient';
		}

		client.subscribe(this.opts.topic, config.broker_subscribe_ops, (err, granted) => {

			//console.log(granted);
		});

	}

	on_message(topic, message) {
		throw 'missing on_message [topic: ' + topic + '] - [message: ' + message + '] - [class: ' + this.constructor.name + ']';
	}

}

module.exports = iSubscriber;












