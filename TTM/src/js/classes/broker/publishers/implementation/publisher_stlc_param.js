"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_stlc_param extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_STLC_PARAM',
	    	topic: '/stlc_param'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_STLC_PARAM Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);
		super.sentToRestService(message, false);				
		super.aggiornaDataOra();		
	}
	

}

module.exports = publisher_stlc_param;