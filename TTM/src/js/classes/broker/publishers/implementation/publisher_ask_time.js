"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_ask_time extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_ASK_TIME',
	    	topic: '/ask_time'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_ASK_TIME Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);		
	}
	

}

module.exports = publisher_ask_time;