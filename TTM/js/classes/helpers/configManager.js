const renderManager = require('./renderManager.js');


module.exports = (() => {

var mainDom=null;

	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _process = (oggetto) => {

		global.deviceConfig=oggetto;

		var layout=oggetto.LayoutSection;
		renderManager.disegna(layout); //disegno i componenti presenti sullo schermo
		renderManager.inserisciContenuti(oggetto.ContentSection); //disegno i componenti presenti sullo schermo




	}		


	var _processExperience = (oggetto) => {
		renderManager.disegnaSceltaExperience(oggetto.experience); //disegno i componenti presenti sullo schermo
	}		


	return {
		process:_process,
		processExperience: _processExperience
	}


})();