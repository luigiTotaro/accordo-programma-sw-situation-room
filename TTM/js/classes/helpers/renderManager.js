const config = require('../../config.js');
const broker = require('../broker/broker.js');


module.exports = (() => {

var mainDom=null;
var mainWindow=null;
var am4core = null;
var am4charts = null;
var am4maps = null;
var am4geodata_worldHigh = null;
var contents4update = new Array();







	var _disegnaSceltaExperience = (contents) => {

		//se il contenuto è vuoto, non mostro la finestra di scelta
		if (contents.length==0)
		{
			var experience2Send = new Object();
			experience2Send.deviceId = config.deviceId;
			experience2Send.experience = "";
			broker.publish('PUBLISHER_CONFIG',experience2Send);
			return;
		}
		else if (contents.length==1) //unica opzione, non mostro la finestra
		{
			var experience2Send = new Object();
			experience2Send.deviceId = config.deviceId;
			experience2Send.experience = contents[0].id;
			broker.publish('PUBLISHER_CONFIG',experience2Send);
			return;
		}

		var dom=mainDom;
		var html="";


		
		html='<div id="divExperience" class="kt-portlet" style="touch-action: none;position:absolute;top:0px;left:'+((global.windowWidth-500)/2)+'px;width:500px;"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-layers-2"></i></span><h3 class="kt-portlet__head-title">Scelta Esperienza</h3></div>';
		html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"></div></div></div>';
		html+='<div id="body_divExperience" class="kt-portlet__body" style="overflow:hidden;">';



		html+='<div class="kt-container   kt-grid__item kt-grid__item--fluid" style="width:100%;">';

		var date = new Date();

		for (var i=0;i<contents.length;i++)
		{


				html+='   <div class="row experienceRow" id="'+contents[i].id+'" style="margin-top:10px;cursor:pointer;">';
				html+='      <div class="col-lg-4"><img src="'+contents[i].image+'?t='+date.getTime()+'" style="width: 130px;height:130px;border: 1px solid #aaaaaa;border-radius: 7px;"></img></div>';
				html+='      <div class="col-lg-8">'+contents[i].name+'</div>';
				html+='   </div>';
                                            
                                      

		}
		html+='</div>';

		html+='</div></div>';					
		dom.append(html);

		//devo centrare la finestra in verticale
		var altezza=mainWindow("#divExperience")[0].clientHeight;
		mainWindow("#divExperience").css("top",((global.windowHeight-altezza)/3)+"px");


		mainWindow( ".experienceRow" ).click(function(event) {
			//nascondo il body
			//console.log(event);
			var id = event.currentTarget.id;
			//var idContainer=id.substring(5);
			//alert(id);

			var experience2Send = new Object();

			experience2Send.deviceId = config.deviceId;
			experience2Send.experience = id;
			//modifico l'id per non creare doppioni
			//content2Send.content.id="TTM_2_"+content2Send.content.id;

			broker.publish('PUBLISHER_CONFIG',experience2Send);

			mainWindow("#divExperience").hide();


/*
			for (var i=0;i<global.contenuti2render.length;i++)
			{
				if (global.contenuti2render[i].id==idContainer)
				{
					//alert(idContainer);
					//alert(JSON.stringify(global.contenuti2render[i]));
					// e lo mando a quel device
					var content2Send = new Object();

					content2Send.deviceId = "TTM_2";
					content2Send.content = global.contenuti2render[i];
					//modifico l'id per non creare doppioni
					//content2Send.content.id="TTM_2_"+content2Send.content.id;

					broker.publish('PUBLISHER_SEND_CONTENT',content2Send);

				}
			}

*/



		});

	}	




	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _disegna = (layout) => {
		//mi devo creare un array di oggetti da disegnare con il parent di ognuno
		var oggetto2render= new Array();
		for (var i=0;i<layout.length;i++)
		{
			var inner=new Object();
			inner.element=layout[i];
			inner.idDom=null;
			oggetto2render.push(inner);
			//ci sono elementi interni?
			var elementi=layout[i].elements;
			if (typeof(elementi)!=undefined)
			{
				for (var t=0;t<elementi.length;t++)
				{
					var inner=new Object();
					inner.element=elementi[t];
					inner.idDom="body_"+layout[i].id;
					oggetto2render.push(inner);
				}
			}
		}
		console.log(oggetto2render);
		global.oggetti2render=oggetto2render;
		//mainWindow("#actualLoadingProgress").css("width","0%");
		
		for (var i=0;i<oggetto2render.length;i++)
		{
			_render(oggetto2render[i]);

			/*
			var percent=Math.floor(((i)/(oggetto2render.length))*100);
			setTimeout(function()
				{ 
					alert(percent);
					mainWindow("#actualLoadingProgress").css("width",percent+"%");
				}, 
			5);*/
		}
		//console.log(mainWindow);
		//mainWindow.test();		
		
		mainWindow( ".closeContainer" ).click(function(event) {
			//nascondo il body
			//console.log(event);
			var id = event.currentTarget.id;
			var idContainer=id.substring(6);
			console.log(idContainer);
			//è chiuso o aperto?
			if (mainWindow("#body_"+idContainer).is(':visible'))
			{
				mainWindow("#body_"+idContainer).hide();
				mainWindow("#"+idContainer).css("height","40px");
				//e cambio l'icona
				mainWindow("#"+id).children("i").attr('class', 'flaticon2-down');
			}
			else
			{
				//quanto era grande questa finestra?
				
				for (var i=0;i<oggetto2render.length;i++)
				{
					if (oggetto2render[i].element.id==idContainer)
					{
						mainWindow("#"+idContainer).css("height",oggetto2render[i].element.height);
						mainWindow("#body_"+idContainer).show();
						//e cambio l'icona
						mainWindow("#"+id).children("i").attr('class', 'flaticon2-up');
					}
				}
			}




		});





	}



	var _render = (oggetto) => {

		var dom=null;
		if (oggetto.idDom==null) dom=mainDom;
		else dom=mainDom.find("#"+oggetto.idDom+"");
		var layout=oggetto.element;
		console.log("Sono dentro _disegna, con parent: " + dom.attr("id"));
		console.log(layout);

		var html="";
		switch(layout.type) {
			case "container":
				//html="<div id='"+layout.id+"' style='position:absolute;top:"+layout.posY+";left:"+layout.posX+";width:"+layout.width+";height:"+layout.height+";"+layout.style+"'></div>";
				//icona:
				var icon="flaticon2-open-box";
				if (layout.icon=="SENSOR") icon="flaticon2-wifi";
				else if (layout.icon=="MESSAGE") icon="flaticon2-email";


				html='<div id="'+layout.id+'" class="kt-portlet zoomable" style="touch-action: none;position:absolute;top:'+layout.posY+';left:'+layout.posX+';width:'+layout.width+';height:'+layout.height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="'+icon+'"></i></span><h3 class="kt-portlet__head-title">'+layout.label+'</h3></div>';
				html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" id="close_'+layout.id+'"  class=" closeContainer btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-up"></i></a></div></div></div>';
				html+='<div id="body_'+layout.id+'"class="kt-portlet__body" ></div></div>';

				//console.log(html);
				dom.append(html);
				console.log("ho disegnato un container, dom: " + dom.attr("id"));

			break;
			case "led":
				var stati=layout.states;
				var initialState=stati[layout.initialState];
				html="<div id='"+layout.id+"' style='position:relative;top:"+layout.posY+";left:"+layout.posX+";'><div name='led' style='border:1px solid #000000;width:"+layout.width+";height:"+layout.width+";border-radius:"+layout.width+";"+initialState+";float:left'></div><p style='margin: 0px;line-height: "+layout.width+";float: left;margin-left: 10px;'>"+layout.label+"</p></div>";
				//console.log(html);
				dom.append(html);
			break;
			case "barra":
				html='<div id="'+layout.id+'" style="position:relative;top:'+layout.posY+';left:'+layout.posX+';"><p style="margin: 0px;">'+layout.label+'</p>';
				html+='<p name="min" style="margin: 0px;float:left">'+layout.minValue+'</p><div class="progress" style="margin-top: 3px;margin-left: 10px;float: left;width:'+layout.width+'"><div name="actual" class="progress-bar progress-bar-striped progress-bar-animated " role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div></div><p name="max" style="margin: 0px;margin-left: 10px;float:left;">'+layout.maxValue+'</p>';
				html+='</div>';
				dom.append(html);
			break;
			case "labelValue":
				html='<div id="'+layout.id+'" style="position:relative;top:'+layout.posY+';left:'+layout.posX+';">';
				html+='<p name="label" style="margin: 0px;float:left;margin-right:10px;">'+layout.label+':</p><p name="value" style="margin: 0px;float:left"></p>';
				html+='</div>';
				dom.append(html);
			break;

			case "textList":
				//html="<div id='"+layout.id+"' style='position:relative;top:"+layout.posY+";left:"+layout.posX+";width:"+layout.width+";height:"+layout.height+";border:1px solid #000000;overflow:auto;'><p style='margin: 10px;'>"+layout.label+"</p><div name='testo' class='kt-notes' style='padding:10px;'></div></div>";
		

				html='<div id="'+layout.id+'" class="kt-portlet" style="position:relative;top:'+layout.posY+';left:'+layout.posX+';width:'+layout.width+';height:'+layout.height+';overflow:auto;"><div class="kt-portlet__head" style="display: none;"><div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Basic Notes</h3></div></div>';
				html+='<div class="kt-portlet__body" ><div class="kt-notes" style=""><div class="kt-notes__items" name="testo"></div></div></div></div>';



				//console.log(html);
				dom.append(html);

			break;
			default:
				// code block
		}

	}		

	var _aggiorna = (oggetto) => {

		//questo oggetto da aggiornare, cosa è?
		if (global.oggetti2render==undefined) return;
		console.log(global.oggetti2render);

		for (var i=0;i<global.oggetti2render.length;i++)
		{
			if (global.oggetti2render[i].element.id!=oggetto.controlId) continue;

			switch(global.oggetti2render[i].element.type) {
				case "led":
					var stati=global.oggetti2render[i].element.states;
					var actualState=stati[oggetto.value];
					//divido sul :
					var separated = actualState.split(":");
					//trovo quel controllo
					mainDom.find("#"+oggetto.controlId+"").children( "[name='led']" ).css(separated[0],separated[1]);
				break;
				case "barra":
					var min=Number(oggetto.valueMin);
					var max=Number(oggetto.valueMax);
					var actual=Number(oggetto.value);
					var percent=Math.floor(((actual-min)/(max-min))*100);

					//trovo quel controllo
					mainDom.find("#"+oggetto.controlId+"").find( "[name='actual']" ).css("width",percent+"%");
					mainDom.find("#"+oggetto.controlId+"").find( "[name='min']" ).html(min);

					//come è la priorità di questo messaggio?
					//il container è aperto?
					if (oggetto.priority==false)
					{
						//non controllo niente
					}
					else
					{
						//se è chiuso, lo riapro
						if (mainDom.find("#"+oggetto.controlId+"").parent().is(':visible'))
						{
							//già aperto, non faccio niente
						}
						else
						{
							//alert("da aprire");
							//devo trovare il container
							var container = mainDom.find("#"+oggetto.controlId+"").parents(".kt-portlet");
							//console.log(container[0].id);
							var closeButton=mainDom.find("#close_"+container[0].id+"");
							//console.log(closeButton);
							closeButton.click();
						}
					}

				break;
				case "textList":
					//devo aggiungere una riga di testo
					var classType="la la-info-circle kt-font-info";
					if (oggetto.msgType=="INFO") classType="la la-info-circle kt-font-info";
					else if (oggetto.msgType=="WARNING") classType="la la-exclamation-circle kt-font-warning";
					else if (oggetto.msgType=="DANGER") classType="la la-exclamation-triangle kt-font-danger";

					var html="";
					html='<div class="kt-notes__item"><div class="kt-notes__media"><span class="kt-notes__icon"><i class="'+classType+'"></i></span></div>';
					html+='<div class="kt-notes__content"><div class="kt-notes__section"><div class="kt-notes__info"><a href="#" class="kt-notes__title">'+oggetto.msgFrom+'</a><span class="kt-notes__desc">'+oggetto.msgTime+'</span></div></div>';
					html+='<span class="kt-notes__body">'+oggetto.msgText+'</span></div></div>';					

					//mainDom.find("#"+oggetto.controlId+"").children( "[name='testo']" ).append(oggetto.value+"<br>");
					mainDom.find("#"+oggetto.controlId+"").find( "[name='testo']" ).append(html);

					//come è la priorità di questo messaggio?
					//il container è aperto?
					if (oggetto.priority==false)
					{
						//non controllo niente
					}
					else
					{
						//se è chiuso, lo riapro
						if (mainDom.find("#"+oggetto.controlId+"").parent().is(':visible'))
						{
							//già aperto, non faccio niente
						}
						else
						{
							//alert("da aprire");
							//devo trovare il container
							var container = mainDom.find("#"+oggetto.controlId+"").parents(".kt-portlet");
							//console.log(container[0].id);
							var closeButton=mainDom.find("#close_"+container[0].id+"");
							//console.log(closeButton);
							closeButton.click();
						}
					}



				break;
				case "labelValue":
					//console.log("labelValue");
					//trovo quel controllo
					mainDom.find("#"+oggetto.controlId+"").children( "[name='value']" ).html(oggetto.value);
				break;

				default:
					// code block
			}
		}

	}	




	var _inserisciContenuti = (contents) => {

		var dom=mainDom;
		global.contenuti2render=contents;

		for (var i=0;i<contents.length;i++)
		{

			if (contents[i].enabled==false) continue;
			var html="";
			
			var w=contents[i].width;
			w=w.substring(0,w.length-2);
			var h=contents[i].height;
			h=h.substring(0,h.length-2);

			switch(contents[i].type) {
				case "video":
					var autoplay="";
					if (contents[i].autoplay==true) autoplay="autoplay";

					var loop="";
					if (contents[i].loop==true) loop="loop";

					var muted="";
					if (contents[i].muted==true) muted="muted";



					html='<div id="'+contents[i].id+'" class="kt-portlet zoomable" data-width="'+w+'" data-height="'+h+'" style="touch-action: none;position:absolute;top:'+contents[i].posY+';left:'+contents[i].posX+';width:'+contents[i].width+';height:'+contents[i].height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-arrow"></i></span><h3 class="kt-portlet__head-title">'+contents[i].label+'</h3></div>';
					html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-gear"></i></a></div></div></div>';
					html+='<div id="body_'+contents[i].id+'" class="kt-portlet__body" style="overflow: hidden;">';

					html+='<video id="videointerno_'+contents[i].id+'" class="" controls '+autoplay+' '+loop+' '+muted+' style="width:100%;position:relative;top:0px;left:0px;">';
					html+="<source src='"+contents[i].src+"' type='video/mp4'/>";
					html+="</video>";

					html+='</div></div>';

					//console.log(html);
					dom.append(html);
					console.log("ho disegnato un video");
					//mainWindow("#body_"+contents[i].id).hide();



				break;
				case "mappa":
					html='<div id="'+contents[i].id+'" class="kt-portlet zoomable" data-width="'+w+'" data-height="'+h+'" style="touch-action: none;position:absolute;top:'+contents[i].posY+';left:'+contents[i].posX+';width:'+contents[i].width+';height:'+contents[i].height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-world"></i></span><h3 class="kt-portlet__head-title">'+contents[i].label+'</h3></div>';
					html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-gear"></i></a></div></div></div>';
					html+='<div id="body_'+contents[i].id+'"class="kt-portlet__body" style="overflow:hidden;">';


					html+='<iframe id="iframeinterno_'+contents[i].id+'" class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="position:relative;top:0;left:0;width:100%;height:'+contents[i].height+'"';
					html+='src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;ll='+contents[i].center+'&amp;t=m&amp;ie=UTF8&amp;hq=&amp;z=15&amp;output=embed&iwloc=near">';
					html+='</iframe>';

					html+='</div></div>';					
					dom.append(html);				
				break;
				case "browser":
					html='<div id="'+contents[i].id+'" class="kt-portlet zoomable" data-width="'+w+'" data-height="'+h+'" style="touch-action: none;position:absolute;top:'+contents[i].posY+';left:'+contents[i].posX+';width:'+contents[i].width+';height:'+contents[i].height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-world"></i></span><h3 class="kt-portlet__head-title">'+contents[i].label+'</h3></div>';
					html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-gear"></i></a></div></div></div>';
					html+='<div id="body_'+contents[i].id+'"class="kt-portlet__body" style="overflow:hidden;">';
					html+='   <div style="position:relative;top:0;left:0;width:100%;"><p style="float:left;margin: 0px;line-height: 25px;">URL</p><input id="urlAddress_'+contents[i].id+'" class="urlAddress" style="float:left;width: 70%;margin-left: 10px;" value="'+contents[i].source+'"><a href="#" id="goUrl_'+contents[i].id+'"  class="goUrl btn btn-clean btn-sm btn-icon btn-icon-md" style="margin-top:-4px;margin-left: 10px;"><i class="flaticon2-right-arrow"></i></a></div>';

					html+='<webview  id="iframeinterno_'+contents[i].id+'" class="" autosize="on"  style="position:relative;top:20;left:0;width:100%;height:'+contents[i].height+'"';
					html+='src="'+contents[i].source+'">';
					html+='</webview >';

					html+='</div></div>';					
					dom.append(html);				
				break;
				case "chart":
					//quale è l'altezza del div?
					var altezza=contents[i].height;
					altezza = altezza.substring(0,altezza.length-2);
					//alert(altezza);

					html='<div id="'+contents[i].id+'" class="kt-portlet zoomable" data-width="'+w+'" data-height="'+h+'" style="touch-action: none;position:absolute;top:'+contents[i].posY+';left:'+contents[i].posX+';width:'+contents[i].width+';height:'+contents[i].height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-graph-2"></i></span><h3 class="kt-portlet__head-title">'+contents[i].label+'</h3></div>';
					html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" id="send_'+contents[i].id+'" class="sendContainer btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-send-1"></i></a></div></div></div>';
					html+='<div id="body_'+contents[i].id+'" class="kt-portlet__body myChartBody" style="overflow:hidden;">';
					html+='   <div id="chartdiv_'+contents[i].id+'" style="width: 100%;height:'+(altezza-30)+'px;"></div>';

					html+='</div></div>';					
					dom.append(html);

					_setChart(contents[i].chartData, contents[i].id);





				break;


				default:
					// code block
			}
		}


		mainWindow( ".urlAddress" ).bind('focusin', function(event){
			
			//console.log("entrato");
			//_entraTastiera(event);

		});

		
		mainWindow( ".goUrl" ).click(function(event) {
			var id = event.currentTarget.id;
			var idContainer=id.substring(6);			
			//alert(idContainer);
			//prendo al nuova url
			var url=mainWindow("#urlAddress_"+idContainer).val();
			//alert(url);
			if ((url.substring(0,7)=="http://") || (url.substring(0,9)=="https://"))
			{
				//ok, l'indirizzo inizia bene
			}
			else
			{
				url="http://"+url;
			}
			mainWindow("#iframeinterno_"+idContainer).attr("src",url);

		});


		mainWindow( ".sendContainer" ).click(function(event) {

			var id = event.currentTarget.id;
			var idContainer=id.substring(5);

			mainWindow("#sfondoContainerSendTo").show();
			//porto in primo piano
			var zMax = 0;
			mainWindow('body *').each(function() {
			  var zIndex = +mainWindow(this).css('zIndex'); // ("+" casts to number or NaN)
			  if (zIndex > zMax) {
			    zMax = zIndex;
			  }
			});
			//questo zindex appartiene all'elemento?
			var myZindex=mainWindow("#sfondoContainerSendTo").css("z-index");
			if (zMax!=myZindex) mainWindow("#sfondoContainerSendTo").css("z-index",zMax+1);
			
			//riempio la tabella dei device a cui posso inviare
			console.log(global.onlineDevice);
			var html="";
			for (var i=0;i<global.onlineDevice.length;i++)
			{
				if (global.onlineDevice[i].online==false) continue;
				if (global.onlineDevice[i].deviceId==config.deviceId) continue;
				//cerco i dettagli di questo device
				var name="";
				for (var t=0;t<global.deviceConfig.HWSection.length;t++)
				{
					if (global.deviceConfig.HWSection[t].id==global.onlineDevice[i].deviceId) name=global.deviceConfig.HWSection[t].name;
				}
				
				
				html+="<button style='width: 60%;margin-left: 20%;margin-bottom: 3%;' onclick=send(\'"+idContainer+"\',\'"+global.onlineDevice[i].deviceId+"\')>"+name+" - "+global.onlineDevice[i].deviceId+"</button>";

			}
			mainWindow("#body_containerSendTo").html(html);

		});


	}	



	var _contentReceived = (content) => {

		var dom=mainDom;
		//lo aggiungo ai contenuti su schermo
		global.contenuti2render.push(content);

		var html="";
		switch(content.type) {
			case "video":

				html='<div id="'+content.id+'" class="" style="position:absolute;top:0px;left:0px;width:100%;height:600px;">';
				html+='<div id="body_'+content.id+'" class="" style="overflow: hidden;">';

				html+='<video id="videointerno_'+content.id+'" class="" controls style="width:100%;position:relative;top:0px;left:0px;">';
				html+="<source src='"+content.src+"' type='video/mp4'/>";
				html+="</video>";

				html+='</div></div>';

				//console.log(html);
				dom.append(html);
				console.log("ho disegnato un video");


				//html='<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="position:absolute;top:300px;left:10px;border:1px solid #000000;width:200px;height:200px;"';
				//html+='src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Sonzini,+25,+Galatina,+LE&amp;aq=0&amp;oq=via+sonzini+25&amp;sll=41.008099,16.727239&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Sonzini,+25,+Galatina,+Lecce,+Puglia&amp;z=14&amp;ll=40.17726,18.169935&amp;output=embed&iwloc=near">';
				//html+='</iframe>';
				//dom.append(html);

			break;
			case "mappa":
				html='<div id="'+content.id+'" class="kt-portlet zoomable" style="touch-action: none;position:absolute;top:'+content.posY+';left:'+content.posX+';width:'+content.width+';height:'+content.height+';"><div class="kt-portlet__head"><div class="kt-portlet__head-label"><span class="kt-portlet__head-icon"><i class="flaticon2-world"></i></span><h3 class="kt-portlet__head-title">'+content.label+'</h3></div>';
				html+='<div class="kt-portlet__head-toolbar"><div class="kt-portlet__head-actions"><a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md"><i class="flaticon2-gear"></i></a></div></div></div>';
				html+='<div id="body_'+content.id+'"class="kt-portlet__body" style="overflow:hidden;">';


				html+='<iframe id="iframeinterno_'+content.id+'" class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="position:relative;top:0;left:0;width:100%;height:'+content.height+'"';
				html+='src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;ll='+content.center+'&amp;t=m&amp;ie=UTF8&amp;hq=&amp;z=15&amp;output=embed&iwloc=near">';
				html+='</iframe>';

				html+='</div></div>';					
				dom.append(html);				
			break;
			case "browser":
				html='<div id="'+content.id+'" class="" style="position:absolute;top:0px;left:0px;width:100%;height:600px;">';
				html+='<div id="body_'+content.id+'" class="" style="overflow: hidden;">';
				html+='   <div style="position:relative;top:0;left:0;width:100%;"><p style="float:left;margin: 0px;line-height: 25px;">URL</p><input id="urlAddress_'+content.id+'" class="urlAddress" style="float:left;width: 70%;margin-left: 10px;" value="'+content.source+'"><a href="#" id="goUrl_'+content.id+'"  class="goUrl btn btn-clean btn-sm btn-icon btn-icon-md" style="margin-top:-4px;margin-left: 10px;"><i class="flaticon2-right-arrow"></i></a></div>';

				html+='<webview  id="iframeinterno_'+content.id+'" class="" autosize="on"  style="position:relative;top:20;left:0;width:100%;height:'+content.height+'"';
				html+='src="'+content.source+'">';
				html+='</webview >';

				html+='</div></div>';					
				dom.append(html);				
			break;
			case "chart":
				html='<div id="'+content.id+'" class="" style="position:absolute;top:0px;left:0px;width:100%;height:1080px;background-color:#ffffff;">';
				html+='<div id="body_'+content.id+'" class="" style="overflow: hidden;">';
				html+='   <div id="chartdiv_'+content.id+'" style="width: 100%;height:1080px;"></div>';

				html+='</div></div>';					
				dom.append(html);

				_setChart(content.chartData, content.id);





			break;


			default:
				// code block
		}


	}


	var _aggiornaContenuti = (oggetto) => {

		//questo contenuto da aggiornare, cosa è?
		console.log("AggiornaContenuti");
		console.log(oggetto);
		console.log(global.contenuti2render);

		for (var ii=0;ii<global.contenuti2render.length;ii++)
		{
			if (global.contenuti2render[ii].id!=oggetto.controlId) continue;

			switch(global.contenuti2render[ii].type) {
				case "chart":

			      console.log("Aggiorno");
			      //var name="Pinot Noir";
			      //var id="CHARTWINE_1";
			      //cerco l'id nell'array dei contenuti aggiornabili
			      console.log(contents4update);
			      for (var i=0;i<contents4update.length;i++)
			      {
			      	if (contents4update[i].id==oggetto.controlId)
			      	{
						console.log("trovato, cerco l'indice riferito a quel nome");
						for (var t=0;t<contents4update[i].chartData.length;t++)
						{
							if (contents4update[i].chartData[t].name==oggetto.item)
							{
								console.log("indice trovato");
								//var value=Math.floor(Math.random() * 400) + 100  ;
								//console.log("Metto il valore: " + value);
								contents4update[i].series.dataItems.getIndex(t).value=oggetto.value;

							}
						}			      		
			      	}
			      }


				break;
				default:
					// code block
			}
		}

	}	


	var _comandaContenuti = (oggetto) => {

		//questo contenuto da aggiornare, cosa è?
		console.log("ComandaContenuti");
		var city=oggetto.value;

		for (var t=0;t<contents4update.length;t++)
		{
			if (contents4update[t].id==oggetto.controlId)
			{
				//trovato l'oggetto da updatare, cosa è?
				console.log(contents4update[t]);
				if (contents4update[t].type=="MapLines")
				{
					var city=oggetto.value;

					var tempData=new Array();
					for (var ii=0;ii<contents4update[t].imageSeries.data.length;ii++)
					{
						for (var i=ii+1;i<contents4update[t].imageSeries.data.length;i++)
						{
							var linecolor = "#aaaaaa";
							var lineopacity = 0.5;
							if ((contents4update[t].imageSeries.data[i].title==city) || (contents4update[t].imageSeries.data[ii].title==city))
							{
								linecolor = "#ff0000";
								lineopacity = 0.8;
							}

							var temp={
							  "multiGeoLine": [
							    [
							      { "latitude": contents4update[t].imageSeries.data[ii].latitude, "longitude": contents4update[t].imageSeries.data[ii].longitude },
							      { "latitude": contents4update[t].imageSeries.data[i].latitude, "longitude": contents4update[t].imageSeries.data[i].longitude }
							    ]
							  ],
							  "lineColor": linecolor,
							  "lineOpacity": lineopacity
							};
							tempData.push(temp);
						}
						contents4update[t].lineSeries.toBack();
						contents4update[t].polygonSeries.toBack();
					}

					contents4update[t].lineSeries.data=tempData;	
					
					//aggiorno anche il grafico radar collegato
					for (var slave_t=0;slave_t<contents4update.length;slave_t++)
					{
						if (contents4update[slave_t].id=="CHARTDETAILSEND_1")
						{
							//creo il nuovo chart.data
							var newData=new Array();
							for (var slave_i=0;slave_i<contents4update[slave_t].chart.data.length;slave_i++)
							{
								var item= new Object();
								item.item=contents4update[slave_t].chart.data[slave_i].item;
								item.value=Math.floor(Math.random() * (200 - 100)) + 100;
								newData.push(item);
							}
							contents4update[slave_t].chart.data=newData;

						}
					}

					//aggiorno anche il grafico lines collegato
					for (var slave_t=0;slave_t<contents4update.length;slave_t++)
					{
						if (contents4update[slave_t].id=="CHARTDETAILSEND_2")
						{
							//creo il nuovo chart.data
							var newData=new Array();
							for (var slave_i=0;slave_i<contents4update[slave_t].chart.data.length;slave_i++)
							{
								var item= new Object();
								item.item=contents4update[slave_t].chart.data[slave_i].item;
								item.value=Math.floor(Math.random() * (220 - 110)) + 110;
								newData.push(item);
							}
							contents4update[slave_t].chart.data=newData;

						}
					}					
				}
				else if (contents4update[t].type=="BulletColumnLine")
				{
					contents4update[t].scrollbarX.start = oggetto.valueStart;
					contents4update[t].scrollbarX.end = oggetto.valueEnd;
					contents4update[t].xAxes.values[0].start = oggetto.valueStart;
					contents4update[t].xAxes.values[0].end = oggetto.valueEnd;
				}


			

			}
		}

	}	

	var _entraTastiera = (event) => {
		mainWindow( ".urlAddress" ).unbind('focusin');
		console.log("Sono in entraTastiera");
		console.log(event);
		var idEdit=event.target.id;
		mainWindow("#hiddenKeyInput").val(idEdit);
		mainWindow( "#virtuaKeyboard" ).show();
	}


	var _setChart = (chartData, chartId) => {

		console.log("Grafico di tipo: " + chartData.type);

		if (chartData.type=="PictorialStackedSeries")
		{
			//devo anche memorizzare i riferimenti a questa chart, per aggiornamenti successivi
			var objTemp = new Object();
			objTemp.id=chartId;


			var iconPath = chartData["iconPath"];

			var chart = am4core.create("chartdiv_"+chartId, am4charts.SlicedChart);
			chart.paddingTop = am4core.percent(chartData["chart.paddingTop"]);

			chart.data = chartData["chart.data"];
			objTemp.chartData=chart.data;

			var series = chart.series.push(new am4charts.PictorialStackedSeries());
			series.dataFields.value = "value";
			series.dataFields.category = "name";
			//series.startLocation = 0.3
			//series.endLocation = 0.85
			objTemp.series=series;


			series.slicesContainer.background.fill = am4core.color(chartData["series.slicesContainer.background.fill"])
			series.slicesContainer.background.fillOpacity = chartData["series.slicesContainer.background.fillOpacity"];
			
			series.maskSprite.path = iconPath;
		
			series.labelsContainer.width = am4core.percent(100);
			series.alignLabels = true;
			series.slices.template.propertyFields.fill = "color";
			series.slices.template.propertyFields.stroke = "color";

			var gradientModifier = new am4core.LinearGradientModifier();
			gradientModifier.lightnesses = chartData["gradientModifier.lightnesses"];
			series.slices.template.fillModifier = gradientModifier;
			series.slices.template.strokeModifier = gradientModifier;

			
			// this makes the fills to start and end at certain location instead of taking whole picture
			series.endLocation = chartData["series.endLocation"];
			series.startLocation = chartData["series.startLocation"];
			// this makes initial fill animation
			series.hiddenState.properties.startLocation = chartData["series.hiddenState.properties.startLocation"];
			series.ticks.template.locationX = chartData["series.ticks.template.locationX"];
			series.labelsContainer.width = chartData["series.labelsContainer.width"];

			contents4update.push(objTemp);


		}
		else if (chartData.type=="MapMorphPie")
		{

			console.log("MapMorphPie");

			var chart = am4core.create("chartdiv_"+chartId, am4maps.MapChart);


			try {
			    chart.geodata = am4geodata_worldHigh;
			}
			catch (e) {
			    chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
			}

			chart.projection = new am4maps.projections.Mercator();

			// zoomout on background click
			chart.chartContainer.background.events.on("hit", function () { zoomOut() });

			var colorSet = new am4core.ColorSet();
			var morphedPolygon;

			// map polygon series (countries)
			var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
			polygonSeries.useGeodata = true;
			// specify which countries to include
			polygonSeries.include = chartData["countries2Include"];

			// country area look and behavior
			var polygonTemplate = polygonSeries.mapPolygons.template;
			polygonTemplate.strokeOpacity = 1;
			polygonTemplate.stroke = am4core.color("#ffffff");
			polygonTemplate.fillOpacity = 0.5;
			polygonTemplate.tooltipText = "{name}";

			// desaturate filter for countries
			var desaturateFilter = new am4core.DesaturateFilter();
			desaturateFilter.saturation = 0.25;
			polygonTemplate.filters.push(desaturateFilter);

			// take a color from color set
			polygonTemplate.adapter.add("fill", function (fill, target) {
			    return colorSet.getIndex(target.dataItem.index + 1);
			})

			// set fillOpacity to 1 when hovered
			var hoverState = polygonTemplate.states.create("hover");
			hoverState.properties.fillOpacity = 1;

			// what to do when country is clicked
			polygonTemplate.events.on("hit", function (event) {
			    event.target.zIndex = 1000000;
			    selectPolygon(event.target);
			})

			// Pie chart
			var pieChart = chart.seriesContainer.createChild(am4charts.PieChart);
			// Set width/heigh of a pie chart for easier positioning only
			pieChart.width = 100;
			pieChart.height = 100;
			pieChart.hidden = true; // can't use visible = false!

			// because defauls are 50, and it's not good with small countries
			pieChart.chartContainer.minHeight = 1;
			pieChart.chartContainer.minWidth = 1;

			var pieSeries = pieChart.series.push(new am4charts.PieSeries());
			pieSeries.dataFields.value = "value";
			pieSeries.dataFields.category = "category";
			//prendere la prima categoria per mettere dei dati di default iniziali
			pieSeries.data = [{ value: 100, category: "First" }, { value: 20, category: "Second" }, { value: 10, category: "Third" }];

			var dropShadowFilter = new am4core.DropShadowFilter();
			dropShadowFilter.blur = 4;
			pieSeries.filters.push(dropShadowFilter);

			var sliceTemplate = pieSeries.slices.template;
			sliceTemplate.fillOpacity = 1;
			sliceTemplate.strokeOpacity = 0;

			var activeState = sliceTemplate.states.getKey("active");
			activeState.properties.shiftRadius = 0; // no need to pull on click, as country circle under the pie won't make it good

			var sliceHoverState = sliceTemplate.states.getKey("hover");
			sliceHoverState.properties.shiftRadius = 0; // no need to pull on click, as country circle under the pie won't make it good

			// we don't need default pie chart animation, so change defaults
			var hiddenState = pieSeries.hiddenState;
			hiddenState.properties.startAngle = pieSeries.startAngle;
			hiddenState.properties.endAngle = pieSeries.endAngle;
			hiddenState.properties.opacity = 0;
			hiddenState.properties.visible = false;

			// series labels
			var labelTemplate = pieSeries.labels.template;
			labelTemplate.nonScaling = true;
			labelTemplate.fill = am4core.color("#FFFFFF");
			labelTemplate.fontSize = 10;
			labelTemplate.background = new am4core.RoundedRectangle();
			labelTemplate.background.fillOpacity = 0.9;
			labelTemplate.padding(4, 9, 4, 9);
			labelTemplate.background.fill = am4core.color("#7678a0");

			// we need pie series to hide faster to avoid strange pause after country is clicked
			pieSeries.hiddenState.transitionDuration = 200;

			
			// country label
			var countryLabel = chart.chartContainer.createChild(am4core.Label);
			countryLabel.text = "Seleziona una nazione";
			countryLabel.fill = am4core.color("#7678a0");
			countryLabel.fontSize = 40;

			countryLabel.hiddenState.properties.dy = 1000;
			countryLabel.defaultState.properties.dy = 0;
			countryLabel.valign = "middle";
			countryLabel.align = "right";
			countryLabel.paddingRight = 50;
			countryLabel.hide(0);
			countryLabel.show();

			// select polygon
			function selectPolygon(polygon) {
			    if (morphedPolygon != polygon) {
			        var animation = pieSeries.hide();
			        if (animation) {
			            animation.events.on("animationended", function () {
			                morphToCircle(polygon);
			            })
			        }
			        else {
			            morphToCircle(polygon);
			        }
			    }
			}

			// fade out all countries except selected
			function fadeOut(exceptPolygon) {
			    for (var i = 0; i < polygonSeries.mapPolygons.length; i++) {
			        var polygon = polygonSeries.mapPolygons.getIndex(i);
			        if (polygon != exceptPolygon) {
			            polygon.defaultState.properties.fillOpacity = 0.5;
			            polygon.animate([{ property: "fillOpacity", to: 0.5 }, { property: "strokeOpacity", to: 1 }], polygon.polygon.morpher.morphDuration);
			        }
			    }
			}

			function zoomOut() {
			    if (morphedPolygon) {
			        pieSeries.hide();
			        morphBack();
			        fadeOut();
			        countryLabel.hide();
			        morphedPolygon = undefined;
			    }
			}

			function morphBack() {
			    if (morphedPolygon) {
			        morphedPolygon.polygon.morpher.morphBack();
			        var dsf = morphedPolygon.filters.getIndex(0);
			        dsf.animate({ property: "saturation", to: 0.25 }, morphedPolygon.polygon.morpher.morphDuration);
			    }
			}

			function morphToCircle(polygon) {


			    var animationDuration = polygon.polygon.morpher.morphDuration;
			    // if there is a country already morphed to circle, morph it back
			    morphBack();
			    // morph polygon to circle
			    polygon.toFront();
			    polygon.polygon.morpher.morphToSingle = true;
			    var morphAnimation = polygon.polygon.morpher.morphToCircle();

			    polygon.strokeOpacity = 0; // hide stroke for lines not to cross countries

			    polygon.defaultState.properties.fillOpacity = 1;
			    polygon.animate({ property: "fillOpacity", to: 1 }, animationDuration);

			    // animate desaturate filter
			    var filter = polygon.filters.getIndex(0);
			    filter.animate({ property: "saturation", to: 1 }, animationDuration);

			    // save currently morphed polygon
			    morphedPolygon = polygon;

			    // fade out all other
			    fadeOut(polygon);

			    // hide country label
			    countryLabel.hide();

			    if (morphAnimation) {
			        morphAnimation.events.on("animationended", function () {
			            zoomToCountry(polygon);
			        })
			    }
			    else {
			        zoomToCountry(polygon);
			    }
			}

			function zoomToCountry(polygon) {
			    var zoomAnimation = chart.zoomToMapObject(polygon, 2.2, true);
			    if (zoomAnimation) {
			        zoomAnimation.events.on("animationended", function () {
			            showPieChart(polygon);
			        })
			    }
			    else {
			        showPieChart(polygon);
			    }
			}


			function showPieChart(polygon) {
			    console.log(polygon);
			    polygon.polygon.measure();
			    var radius = polygon.polygon.measuredWidth / 2 * polygon.globalScale / chart.seriesContainer.scale;
			    pieChart.width = radius * 2;
			    pieChart.height = radius * 2;
			    pieChart.radius = radius;

			    var centerPoint = am4core.utils.spritePointToSvg(polygon.polygon.centerPoint, polygon.polygon);
			    centerPoint = am4core.utils.svgPointToSprite(centerPoint, chart.seriesContainer);

			    pieChart.x = centerPoint.x - radius;
			    pieChart.y = centerPoint.y - radius;

			    var fill = polygon.fill;
			    var desaturated = fill.saturate(0.3);

			    var countryData=[{ value: 100, category: "First" }, { value: 100, category: "Second" }, { value: 100, category: "Third" }]
			    for (var i=0;i<chartData["chart.data"].length;i++)
			    {
			    	if (chartData["chart.data"][i].name==polygon.dataItem.dataContext.id)
			    	{
						countryData=chartData["chart.data"][i].value;
			    	}
			    }

			    for (var i = 0; i < pieSeries.dataItems.length; i++) {
			        var dataItem = pieSeries.dataItems.getIndex(i);
			        dataItem.category = countryData[i].category;
			        dataItem.value = countryData[i].value;
			        

			        //dataItem.value = Math.round(Math.random() * 100);
			        dataItem.slice.fill = am4core.color(am4core.colors.interpolate(
			            fill.rgb,
			            am4core.color("#ffffff").rgb,
			            0.2 * i
			        ));

			        dataItem.label.background.fill = desaturated;
			        dataItem.tick.stroke = fill;
			    }

			    pieSeries.show();
			    pieChart.show();

			    countryLabel.text = "{name}";
			    countryLabel.dataItem = polygon.dataItem;
			    countryLabel.fill = desaturated;
			    countryLabel.show();
			}



		}
		else if (chartData.type=="MapLines")
		{

			console.log("MapLines");



			// Define marker path
			var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

			// Create map instance
			var chart = am4core.create("chartdiv_"+chartId, am4maps.MapChart);
			var interfaceColors = new am4core.InterfaceColorSet();

			// Set map definition
			chart.geodata = am4geodata_worldLow;

			// Set projection
			chart.projection = new am4maps.projections.Mercator();

			// Add zoom control
			chart.zoomControl = new am4maps.ZoomControl();

			// Set initial zoom
			chart.homeZoomLevel = 2.5;
			chart.homeGeoPoint = {
			  latitude: 51,
			  longitude: -23
			};

			// Create map polygon series
			var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
			polygonSeries.exclude = ["AQ"];
			polygonSeries.useGeodata = true;
			polygonSeries.mapPolygons.template.nonScalingStroke = true;

			// Add images
			var imageSeries = chart.series.push(new am4maps.MapImageSeries());
			var imageTemplate = imageSeries.mapImages.template;
			imageTemplate.tooltipText = "{title}";
			imageTemplate.nonScaling = true;

			var marker = imageTemplate.createChild(am4core.Sprite);
			marker.path = targetSVG;
			marker.horizontalCenter = "middle";
			marker.verticalCenter = "middle";
			marker.scale = 0.7;
			marker.fill = interfaceColors.getFor("alternativeBackground");

			marker.events.on("hit", function(event) {
			    //alert(event.target.parent.dataItem.dataContext.title);
				selectCity(event.target.parent.dataItem.dataContext.title);
			    //showLines(event.target.dataItem);
			})

			imageTemplate.propertyFields.latitude = "latitude";
			imageTemplate.propertyFields.longitude = "longitude";

			imageSeries.data = chartData["chart.data"];

			// Add lines
			var lineSeries = chart.series.push(new am4maps.MapArcSeries());
			lineSeries.dataFields.multiGeoLine = "multiGeoLine";

			var lineTemplate = lineSeries.mapLines.template;
			lineTemplate.nonScalingStroke = false;
			lineTemplate.stroke = interfaceColors.getFor("alternativeBackground");
			lineTemplate.fill = interfaceColors.getFor("alternativeBackground");
			//lineTemplate.line.strokeOpacity = 0.2;
			lineTemplate.propertyFields.stroke = "lineColor";
			lineTemplate.line.propertyFields.strokeOpacity = "lineOpacity";

			lineTemplate.line.controlPointDistance = -0.1;
			lineTemplate.line.controlPointPosition = 0.6;
			//lineTemplate.line.tensionX = 1.0;
			//lineTemplate.line.tensionY = 1.0;

			var objTemp = new Object();
			objTemp.id=chartId;
			objTemp.type=chartData.type;
			objTemp.imageSeries=imageSeries;
			objTemp.lineSeries=lineSeries;
			objTemp.polygonSeries=polygonSeries;
			contents4update.push(objTemp);

			//creo le linee

			lineSeries.data=new Array();
			/*
			for (var ii=0;ii<0;ii++)
			{

				for (var i=ii+1;i<imageSeries.data.length;i++)
				{
					var temp={
					  "multiGeoLine": [
					    [
					      { "latitude": imageSeries.data[ii].latitude, "longitude": imageSeries.data[ii].longitude },
					      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
					    ]
					  ]
					};
					lineSeries.data.push(temp);
				}
			}
*/

			
			var endIndex=1;
			
			setTimeout(function()
			{ 

			var initialAnim = setInterval(function()
				{ 

					if (endIndex<=imageSeries.data.length)
					{
						var tempData=new Array();
						for (var ii=0;ii<endIndex;ii++)
						{
							
							for (var i=ii+1;i<imageSeries.data.length;i++)
							{
								if (ii==i) continue;
								var temp={
								  "multiGeoLine": [
								    [
								      { "latitude": imageSeries.data[ii].latitude, "longitude": imageSeries.data[ii].longitude },
								      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
								    ]
								  ],
								  "lineColor": "#aaaaaa",
								  "lineOpacity": 0.4
								};
								tempData.push(temp);
							}

						}
						lineSeries.data=tempData;	

						//console.log(lineSeries.data);
						endIndex++;
						lineSeries.toBack();
						polygonSeries.toBack();
					}
					else
					{
						//polygonSeries.toBack();
						clearInterval(initialAnim);					
					}
					//arrow.position = arrow.position+0.01;
					//if (arrow.position>=1) arrow.position=1

					//lineTemplate.line.controlPointDistance = lineTemplate.line.controlPointDistance-0.01;

					//lineTemplate.line.controlPointDistance = -0.1;
					//lineTemplate.line.controlPointPosition = 0.6;					

					/*var tempData=new Array();
					for (var ii=0;ii<imageSeries.data.length;ii++)
					{

						for (var i=ii+1;i<imageSeries.data.length;i++)
						{
							var temp={
							  "multiGeoLine": [
							    [
							      { "latitude": imageSeries.data[ii].latitude, "longitude": imageSeries.data[ii].longitude },
							      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
							    ]
							  ]
							};
							tempData.push(temp);
						}

					}*/
					//lineSeries.data=tempArray;	
					//lineSeries.reinit();
					//lineSeries.invalidateRawData()	;			
				}, 
			50);


				}, 
			2000);

			function selectCity(city) {
				console.log("selectCity");
				
				//trovo l'indice della città


				var tempData=new Array();
				//li faccio tutti escluso quello selezionato, poi fccio solo il selezionato
				//var index=-1;
				for (var ii=0;ii<imageSeries.data.length;ii++)
				{


					for (var i=ii+1;i<imageSeries.data.length;i++)
					{

						var linecolor = "#aaaaaa";
						var lineopacity = 0.5;
						if ((imageSeries.data[i].title==city) || (imageSeries.data[ii].title==city))
						{

							linecolor = "#ff0000";
							lineopacity = 0.8;
						}

						var temp={
						  "multiGeoLine": [
						    [
						      { "latitude": imageSeries.data[ii].latitude, "longitude": imageSeries.data[ii].longitude },
						      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
						    ]
						  ],
						  "lineColor": linecolor,
						  "lineOpacity": lineopacity
						};
						tempData.push(temp);
					}
					lineSeries.toBack();
					polygonSeries.toBack();
				}
				/*
				var linecolor = "#ff0000";
				var lineopacity = 0.8;
				for (var i=0;i<imageSeries.data.length;i++)
				{
					if (index==i) continue;
					var temp={
					  "multiGeoLine": [
					    [
					      { "latitude": imageSeries.data[index].latitude, "longitude": imageSeries.data[index].longitude },
					      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
					    ]
					  ],
					  "lineColor": linecolor,
					  "lineOpacity": lineopacity
					};
					tempData.push(temp);
				}
				lineSeries.toBack();
				polygonSeries.toBack();
*/

				lineSeries.data=tempData;	

				// e propago l'evento, in caso ho duplicazioni di questo controllo
				var updateObj=new Object();
				//updateObj.deviceId="TTM_1";
				updateObj.type="CONTROL_CONTENT";
				updateObj.controlId=chartId;
				updateObj.subType="SELECT_CITY";
				updateObj.value=city;
				broker.publish('PUBLISHER_EVENT',updateObj);


			}			

/*
			setTimeout(function()
				{ 

					var tempData=new Array();
					for (var ii=0;ii<endIndex;ii++)
					{
						var linecolor = "#aaaaaa";
						if (ii==0) linecolor = "#ff0000";

						var lineopacity = 0.5;
						if (ii==0) lineopacity = 0.8;


						for (var i=ii+1;i<imageSeries.data.length;i++)
						{
							var temp={
							  "multiGeoLine": [
							    [
							      { "latitude": imageSeries.data[ii].latitude, "longitude": imageSeries.data[ii].longitude },
							      { "latitude": imageSeries.data[i].latitude, "longitude": imageSeries.data[i].longitude }
							    ]
							  ],
							  "lineColor": linecolor,
							  "lineOpacity": lineopacity
							};
							tempData.push(temp);
						}

					}
					lineSeries.data=tempData;	

					console.log(lineSeries.data);
					endIndex++;

					//arrow.position = arrow.position+0.01;
					//if (arrow.position>=1) arrow.position=1

					//lineTemplate.line.controlPointDistance = lineTemplate.line.controlPointDistance-0.01;

					//lineTemplate.line.controlPointDistance = -0.1;
					//lineTemplate.line.controlPointPosition = 0.6;					

					//lineSeries.data=tempArray;	
					//lineSeries.reinit();
					//lineSeries.invalidateRawData()	;			
				}, 
			5000999);

*/



		}
		else if (chartData.type=="BulletColumnLine")
		{

			var wait4timer = false; //se false, istanzia il timeout per spedire il valore, altrimenti ignora l'evewnto fino a che non ha spedito il valore

			var chart = am4core.create("chartdiv_"+chartId, am4charts.XYChart);

			chart.data =chartData["chart.data"];
			
			// Create axes
			var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			//dateAxis.renderer.grid.template.location = 0;
			//dateAxis.renderer.minGridDistance = 30;

			var valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis1.title.text = "Sales";

			var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis2.title.text = "Market Days";
			valueAxis2.renderer.opposite = true;
			valueAxis2.renderer.grid.template.disabled = true;

			// Create series
			var series1 = chart.series.push(new am4charts.ColumnSeries());
			series1.dataFields.valueY = "sales1";
			series1.dataFields.dateX = "date";
			series1.yAxis = valueAxis1;
			series1.name = "Target Sales";
			series1.tooltipText = "{name}\n[bold font-size: 20]${valueY}M[/]";
			series1.fill = chart.colors.getIndex(0);
			series1.strokeWidth = 0;
			series1.clustered = false;
			series1.columns.template.width = am4core.percent(40);

			var series2 = chart.series.push(new am4charts.ColumnSeries());
			series2.dataFields.valueY = "sales2";
			series2.dataFields.dateX = "date";
			series2.yAxis = valueAxis1;
			series2.name = "Actual Sales";
			series2.tooltipText = "{name}\n[bold font-size: 20]${valueY}M[/]";
			series2.fill = chart.colors.getIndex(0).lighten(0.5);
			series2.strokeWidth = 0;
			series2.clustered = false;
			series2.toBack();

			var series3 = chart.series.push(new am4charts.LineSeries());
			series3.dataFields.valueY = "market1";
			series3.dataFields.dateX = "date";
			series3.name = "Market Days";
			series3.strokeWidth = 2;
			series3.tensionX = 0.7;
			series3.yAxis = valueAxis2;
			series3.tooltipText = "{name}\n[bold font-size: 20]{valueY}[/]";

			var bullet3 = series3.bullets.push(new am4charts.CircleBullet());
			bullet3.circle.radius = 3;
			bullet3.circle.strokeWidth = 2;
			bullet3.circle.fill = am4core.color("#fff");

			var series4 = chart.series.push(new am4charts.LineSeries());
			series4.dataFields.valueY = "market2";
			series4.dataFields.dateX = "date";
			series4.name = "Market Days ALL";
			series4.strokeWidth = 2;
			series4.tensionX = 0.7;
			series4.yAxis = valueAxis2;
			series4.tooltipText = "{name}\n[bold font-size: 20]{valueY}[/]";
			series4.stroke = chart.colors.getIndex(0).lighten(0.5);
			series4.strokeDasharray = "3,3";

			var bullet4 = series4.bullets.push(new am4charts.CircleBullet());
			bullet4.circle.radius = 3;
			bullet4.circle.strokeWidth = 2;
			bullet4.circle.fill = am4core.color("#fff");

			// Add cursor
			chart.cursor = new am4charts.XYCursor();

			// Add legend
			chart.legend = new am4charts.Legend();
			chart.legend.position = "top";

			// Add scrollbar
			chart.scrollbarX = new am4charts.XYChartScrollbar();
			chart.scrollbarX.series.push(series1);
			chart.scrollbarX.series.push(series3);
			chart.scrollbarX.parent = chart.bottomAxesContainer;

			var objTemp = new Object();
			objTemp.id=chartId;
			objTemp.type=chartData.type;
			objTemp.scrollbarX=chart.scrollbarX;
			objTemp.xAxes=chart.xAxes;
			contents4update.push(objTemp);



			chart.scrollbarX.events.on("rangechanged", function(event) {
				if (!wait4timer)
				{
					wait4timer=true;
					setTimeout(function()
					{ 
						//ho start e end della barra, ma devo calcolare anche start e end per il grafico
						//var graphMin=(min+(delta*chart.scrollbarX.start));
						//var graphMax=(min+(delta*chart.scrollbarX.end));

						wait4timer=false;
						console.log(chart.scrollbarX.end);
						// e propago l'evento, in caso ho duplicazioni di questo controllo
						var updateObj=new Object();
						//updateObj.deviceId="TTM_1";
						updateObj.type="CONTROL_CONTENT";
						updateObj.controlId=chartId;
						updateObj.subType="CHANGE_RANGE";
						updateObj.valueStart=chart.scrollbarX.start;
						updateObj.valueEnd=chart.scrollbarX.end;
						//updateObj.valueGraphStart=graphMin;
						//updateObj.valueGraphEnd=graphMax;
						broker.publish('PUBLISHER_EVENT',updateObj);

					}, 
					2000);
				}
				//console.log(event.target);
			})

		}
		else if (chartData.type=="Radar")
		{
			var chart = am4core.create("chartdiv_"+chartId, am4charts.RadarChart);

			/* Add data */
			chart.data = chartData["chart.data"];

			/* Create axes */
			var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
			categoryAxis.dataFields.category = "item";

			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
			valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
			valueAxis.max = 200;
			valueAxis.min = 000;

			/* Create and configure series */
			var series = chart.series.push(new am4charts.RadarSeries());
			series.dataFields.valueY = "value";
			series.dataFields.categoryX = "item";
			series.name = "Sms Sent";
			series.strokeWidth = 3;
			series.fillOpacity = 0.2;

			var objTemp = new Object();
			objTemp.id=chartId;
			objTemp.type=chartData.type;
			objTemp.chart=chart;
			contents4update.push(objTemp);


		}
		else if (chartData.type=="Lines")
		{
			var chart = am4core.create("chartdiv_"+chartId, am4charts.XYChart);

			/* Add data */
			chart.data = chartData["chart.data"];

			// Set input format for the dates
			chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

			// Create axes
			var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis.max = 250;
			valueAxis.min = 50;

			// Create series
			var series = chart.series.push(new am4charts.LineSeries());
			series.dataFields.valueY = "value";
			series.dataFields.dateX = "item";
			series.tooltipText = "{value}"
			series.strokeWidth = 2;
			series.minBulletDistance = 15;


			// Make bullets grow on hover
			var bullet = series.bullets.push(new am4charts.CircleBullet());
			bullet.circle.strokeWidth = 2;
			bullet.circle.radius = 4;
			bullet.circle.fill = am4core.color("#fff");



			var objTemp = new Object();
			objTemp.id=chartId;
			objTemp.type=chartData.type;
			objTemp.chart=chart;
			contents4update.push(objTemp);


		}



	}




	var _setDom = (dom) => {  
		mainDom = dom;
	};  


	var _setMain = (main) => {  
		mainWindow = main;
	}; 

	var _setChartCore = (core,chart,maps,geodata_worldHigh,geodata_worldLow) => {  
		console.log("setChartCore");
		am4core = core;
		am4charts = chart;
		am4maps = maps;
		am4geodata_worldHigh = geodata_worldHigh;
		am4geodata_worldLow = geodata_worldLow;
		console.log(am4core);
		console.log(am4charts);
		console.log(am4maps);
		console.log(am4geodata_worldHigh);
		console.log(am4geodata_worldLow);



	}; 

	return {
		disegnaSceltaExperience:_disegnaSceltaExperience,
		disegna:_disegna,
		aggiorna:_aggiorna,
		aggiornaContenuti:_aggiornaContenuti,
		comandaContenuti:_comandaContenuti,
		inserisciContenuti:_inserisciContenuti,
		contentReceived:_contentReceived,
		entraTastiera:_entraTastiera,
		setDom:_setDom,
		setMain:_setMain,
		setChartCore:_setChartCore
	}


})();