const renderManager = require('./renderManager.js');


module.exports = (() => {


	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _process = (oggetto) => {

		switch(oggetto.type) {
			case "CHANGE_VALUE":
				renderManager.aggiorna(oggetto);
			break;
			case "CHANGE_VALUE_CONTENT":
				renderManager.aggiornaContenuti(oggetto);
			break;
			case "CONTROL_CONTENT":
				renderManager.comandaContenuti(oggetto);
			break;
			default:
				// code block
		}




	}		




	return {
		process:_process
	}


})();