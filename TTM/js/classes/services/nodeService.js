"use strict";
const config = require('../../config.js');
const broker = require('../broker/broker.js');

module.exports = (() => {

	/** 
 	* @function 'initialize' - Questa funzione chiama l'initializa della classe Agent, che recupera il Server ID dal db
 	* @param {callback} on_complete - function(err)
 	*/
	 var _initialize = (on_complete) => {

		console.log("Agentservice initialize");
		on_complete(null);
	}


	var _isAlive = () => {

		var data2Send = new Object();
		data2Send.deviceId = config.deviceId;
		broker.publish('PUBLISHER_IS_ALIVE',data2Send);

	}	

	return {
		initialize:_initialize,
		isAlive:_isAlive
	}


})();