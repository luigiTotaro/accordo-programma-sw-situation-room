"use strict";
const iPublisher = require('../iPublisher.js');

class publisher_config extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_CONFIG',
	    	topic: '/config'
	    });
	}

	publish(client, message) {
		
		console.log("PUBLISHER_CONFIG Invio messaggio.....");
		super.publish(client, message);		
	}
	

}

module.exports = publisher_config;