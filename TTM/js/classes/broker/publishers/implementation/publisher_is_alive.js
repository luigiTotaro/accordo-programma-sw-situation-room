"use strict";
const iPublisher = require('../iPublisher.js');


class publisher_is_alive extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_IS_ALIVE',
	    	topic: '/is_alive'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_IS_ALIVE Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_is_alive;