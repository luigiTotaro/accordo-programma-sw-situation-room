"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_experience extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_EXPERIENCE',
	    	topic: '/experience'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_EXPERIENCE Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_experience;