"use strict";

const iSubscriber = require('../iSubscriber.js');
const eventManager = require('../../../helpers/eventManager.js');

class subscriber_event extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/event'
		});
		console.log('subscriber_event istanziato');
	}

	on_message(topic, message) {
		console.log('subscriber_event event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		eventManager.process(messaggio);
	}

}

module.exports = subscriber_event;