var cytoArray = new Array();
var cy = null;

      global.getCyto = function (container, id) {

        cy = cytoscape({
          container: document.getElementById(container),

          layout: {
            name: 'avsdf',
            nodeSeparation: 120
          },

          style: [
            {
              selector: 'node',
              style: {
                'label': 'data(id)',
                'text-valign': 'center',
                'color': '#000000',
                'background-color': '#3a7ecf'
              }
            },

            {
              selector: 'edge',
              style: {
                'width': 2,
                'line-color': '#3a7ecf',
                'opacity': 0.5
              }
            }
          ]/*,

          elements: {
            nodes: [
              { data: { id: 'v1', weight: 1} },
              { data: { id: 'v2', weight: 2} },
              { data: { id: 'v3', weight: 3} },
              { data: { id: 'v4', weight: 4} },
              { data: { id: 'v5', weight: 5} },
              { data: { id: 'v6', weight: 6} },
              { data: { id: 'v7', weight: 7} }
            ],
            edges: [
              { data: { source: 'v1', target: 'v2', directed: 'false'} },
              { data: { source: 'v1', target: 'v4', directed: 'false'} },
              { data: { source: 'v1', target: 'v5', directed: 'false'} },
              { data: { source: 'v2', target: 'v4', directed: 'false'} },
              { data: { source: 'v2', target: 'v6', directed: 'false'} },
              { data: { source: 'v3', target: 'v4', directed: 'false'} },
              { data: { source: 'v3', target: 'v7', directed: 'false'} },
              { data: { source: 'v4', target: 'v5', directed: 'false'} },
              { data: { source: 'v4', target: 'v7', directed: 'false'} },
              { data: { source: 'v5', target: 'v6', directed: 'false'} },
              { data: { source: 'v6', target: 'v7', directed: 'false'} },
              { data: { source: 'v6', target: 'v3', directed: 'false'} }
            ]
          }*/
        });

        var eles = cy.add([
              { group: 'nodes', data: { id: 'n0' }, position: { x: 100, y: 100 } },
              { group: 'nodes', data: { id: 'n1' }, position: { x: 200, y: 200 } },
              { group: 'edges', data: { id: 'e0', source: 'n0', target: 'n1' } }
            ]);

        var obj = new Object();
        obj.id=id;
        obj.cy=cy;
        cytoArray.push(obj);


            var data = [
            { group: 'nodes', data: { id: 'n2' }, position: { x: 400, y: 400 } },
            { group: 'nodes', data: { id: 'n3' }, position: { x: 500, y: 500 } },
            { group: 'edges', data: { id: 'e1', source: 'n2', target: 'n3' } }
          ];
          //global.setCytoData(id,JSON.stringify(data));

      }


      global.setCytoData = function (id,data) {
        
        console.log("setCytoData");
        console.log(id);
        console.log(data);

        //alert(id);
       //var cy = global.cy
       //cy.add(data);
          var casuale=Math.floor(Math.random() * (5000 - 0)) + 0;
        
/*
      for (var i = 0; i < 10; i++) {
          var posx=Math.floor(Math.random() * (500 - 20)) + 20;
          var posy=Math.floor(Math.random() * (500 - 20)) + 20;


          cy.add({
              data: { id: 'node_' + casuale + i }, position: { x: posx, y: posy }
              }
          );
          var source = 'node_' + casuale + i;
          cy.add({
              data: {
                  id: 'edge_' + casuale + i,
                  source: source,
                  target: (i % 2 == 0 ? 'n0' : 'n1')
              }
          });
      }
*/

/*
        //cerco il riferimento a cy in base all'id
        for (var i = 0; i < cytoArray.length; i++) {
          if (cytoArray[i].id==id)
          {
            var cy = cytoArray[i].cy;
            var eles = cy.add(data);
          }
        }        
  */   

  cy.add(JSON.parse(data));   
        

      }
