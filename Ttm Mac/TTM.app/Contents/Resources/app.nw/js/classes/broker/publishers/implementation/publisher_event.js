"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_event extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_EVENT',
	    	topic: '/event'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_EVENT Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_event;