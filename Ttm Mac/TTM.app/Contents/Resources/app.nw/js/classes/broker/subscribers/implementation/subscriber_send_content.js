"use strict";

const iSubscriber = require('../iSubscriber.js');
const renderManager = require('../../../helpers/renderManager.js');
const config = require('../../../../config.js');


class subscriber_send_content extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/sendContent'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_send_content event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		//è per me?
		if (messaggio.deviceId==config.deviceId) renderManager.contentReceived(messaggio.content);
	}

}

module.exports = subscriber_send_content;