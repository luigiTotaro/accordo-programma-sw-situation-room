"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_request_data extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_REQUESTDATA',
	    	topic: '/request_data'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_REQUESTDATA Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);		
	}
	

}

module.exports = publisher_request_data;