"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_send_content extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_SEND_CONTENT',
	    	topic: '/sendContent'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_SEND_CONTENT Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_send_content;