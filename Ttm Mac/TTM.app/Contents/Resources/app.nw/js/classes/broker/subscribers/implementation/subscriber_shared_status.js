"use strict";

const iSubscriber = require('../iSubscriber.js');

class subscriber_shared_status extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/sharedStatus'
		});
		console.log('subscriber_shared_status istanziato');
	}

	on_message(topic, message) {
		console.log('subscriber_shared_status event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		global.onlineDevice=messaggio;
		console.log(global.onlineDevice);
	}

}

module.exports = subscriber_shared_status;