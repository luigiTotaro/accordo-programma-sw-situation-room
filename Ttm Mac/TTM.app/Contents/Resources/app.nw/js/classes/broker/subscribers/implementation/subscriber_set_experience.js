"use strict";

const iSubscriber = require('../iSubscriber.js');
const configManager = require('../../../helpers/configManager.js');
const config = require('../../../../config.js');


class subscriber_set_experience extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/set_experience'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_set_experience event', topic, message.toString());
		//alert(message);
		configManager.setExperience(message.toString());

	}

}

module.exports = subscriber_set_experience;