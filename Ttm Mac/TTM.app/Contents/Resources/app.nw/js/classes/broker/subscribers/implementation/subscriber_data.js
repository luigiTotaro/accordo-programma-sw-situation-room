"use strict";

const iSubscriber = require('../iSubscriber.js');
const renderManager = require('../../../helpers/renderManager.js');

class subscriber_data extends iSubscriber {

	
	constructor() {
	    super({
	    	topic: '/data'
		});
		console.log('subscriber_data istanziato');
	}

	on_message(topic, message) {
		console.log('subscriber_data event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		renderManager.dataArrived(messaggio);
	}

}

module.exports = subscriber_data;