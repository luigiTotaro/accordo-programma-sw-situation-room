"use strict";

const iSubscriber = require('../iSubscriber.js');
const configManager = require('../../../helpers/configManager.js');
const config = require('../../../../config.js');


class subscriber_experience_reply extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/experience_reply'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_experience_reply event', topic, message.toString());
		var messaggio=JSON.parse(message.toString());
		//� per me?
		if (messaggio.deviceId==config.deviceId) configManager.processExperience(messaggio);
	}

}

module.exports = subscriber_experience_reply;