"use strict";
const iPublisher = require('../iPublisher.js');


class publisher_is_init extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_IS_INIT',
	    	topic: '/is_init'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_IS_INIT Invio messaggio.....");
		super.publish(client, message);
	}
	

}

module.exports = publisher_is_init;