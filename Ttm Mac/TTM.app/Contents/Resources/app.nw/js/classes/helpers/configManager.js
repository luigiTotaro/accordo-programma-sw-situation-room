const renderManager = require('./renderManager.js');
const nodeService = require('../services/nodeService.js');
const config = require('../../config.js');
const broker = require('../broker/broker.js');

module.exports = (() => {

var mainDom=null;

	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _process = (oggetto) => {

		global.deviceConfig=oggetto;

		config.name = oggetto.HWSection[0].name;
		config.type = oggetto.HWSection[0].type;
		config.role = oggetto.HWSection[0].role;
		nodeService.isInit();

		var layout=oggetto.LayoutSection;
		renderManager.disegna(layout); //disegno i componenti presenti sullo schermo
		renderManager.inserisciContenuti(oggetto.ContentSection); //disegno i componenti presenti sullo schermo

	}		


	var _processExperience = (oggetto) => {
		renderManager.disegnaSceltaExperience(oggetto.experience); //disegno i componenti presenti sullo schermo
	}		

	var _setExperience = (experience) => {
		//carico questo config, ma solo se sono slave!
		if (config.role=="SLAVE")
		{
			var experience2Send = new Object();
			experience2Send.deviceId = config.deviceId;
			experience2Send.experience = experience;
			broker.publish('PUBLISHER_CONFIG',experience2Send);	
		}
	}	

	return {
		process:_process,
		processExperience: _processExperience,
		setExperience: _setExperience
	}


})();