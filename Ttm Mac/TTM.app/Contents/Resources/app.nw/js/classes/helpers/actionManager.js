const config = require('../../config.js');

module.exports = (() => {

var mainDom=null;
var mainWindow=null;

	/** 
 	* @function 'process' - processa il file config ricevuto
 	* @param {object} oggetto - oggetto config contenente tutti i dati
 	*/
	var _process = (oggetto, action) => {

		console.log("****************");
		console.log(oggetto);
		console.log(action);
		console.log("****************");

		mainWindow("#sfondoContainerSendTo").show();
		//porto in primo piano
		var zMax = 0;
		mainWindow('body *').each(function() {
		  var zIndex = +mainWindow(this).css('zIndex'); // ("+" casts to number or NaN)
		  if (zIndex > zMax) {
		    zMax = zIndex;
		  }
		});
		//questo zindex appartiene all'elemento?
		var myZindex=mainWindow("#sfondoContainerSendTo").css("z-index");
		if (zMax!=myZindex) mainWindow("#sfondoContainerSendTo").css("z-index",zMax+1);
		
		//riempio la tabella dei device a cui posso inviare
		console.log(global.onlineDevice);
		var html="";
		for (var i=0;i<global.onlineDevice.length;i++)
		{
			if (global.onlineDevice[i].online==false) continue;
			if (global.onlineDevice[i].deviceId==config.deviceId) continue;
			//cerco i dettagli di questo device
			//var name="";
			//for (var t=0;t<global.deviceConfig.HWSection.length;t++)
			//{
			//	if (global.deviceConfig.HWSection[t].id==global.onlineDevice[i].deviceId) name=global.deviceConfig.HWSection[t].name;
			//}
			
			
			html+="<button style='width: 60%;margin-left: 20%;margin-bottom: 3%;' onclick=send(\'"+oggetto+"\',\'"+global.onlineDevice[i].deviceId+"\')>"+global.onlineDevice[i].name+" - "+global.onlineDevice[i].deviceId+"</button>";

		}
		mainWindow("#body_containerSendTo").html(html);






	}		


	var _setDom = (dom) => {  
		mainDom = dom;
	};  


	var _setMain = (main) => {  
		mainWindow = main;
	}; 

	return {
		process:_process,
		setDom:_setDom,
		setMain:_setMain

	}


})();