"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const nodeService = require('../../services/nodeService.js');
//const logger = require('../../utils/logger.js');


class ScheduleCommandIsAlive extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/isAlive'
	    });
	}

	run() {
		//logger.log('info', '--- Scheduler isAlive');
		
		nodeService.isAlive((err) => {
			if (err)
			{
				//logger.log('error',err);
			}
		});
			
	}

}

module.exports = ScheduleCommandIsAlive;