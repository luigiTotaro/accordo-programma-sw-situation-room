"use strict";
const db_conn = require('./db_connection.js');

module.exports = {
	

	test_sql: function(sql, on_complete) {

		db_conn.select("SELECT * FROM stlc_parameters", (err, result) => {

			if(err) {
				on_complete(err, null);
			} else {
				on_complete(null, result)
			}

		}); 
	},

	/** 
 	* @function 'getSTLCData' - Recupera i dati dalla tabella stlc_parameters
 	* @param {callback} on_complete - function(err, result)
 	*/	
	getSTLCData: function(on_complete) {

		var ret = new Object(); 
		db_conn.select("SELECT * FROM stlc_parameters", (err, result) => {
			if (err) result=null;
			ret.stlc_param = result;
			on_complete(err, ret)

		}); 
	},

	/** 
 	* @function 'getServerData' - Recupera i dati dalla tabella servers
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 getServerData: function(sql, on_complete) {

		db_conn.select("SELECT * FROM servers", (err, result) => {
			
			if(err) {
				on_complete(err, null); 
			} else {
				on_complete(null, result)
			}        	

		}); 
	},

	/** 
 	* @function 'getServerData' - Recupera i dati dalle tabelle servers, devices, rack, building, station, port
 	* @param {callback} on_complete - function(err, result)
 	*/	
	getConfigData: function(on_complete) {

		let select_lst = [
			"SELECT * FROM servers",
			"SELECT * FROM devices",
			"SELECT * FROM rack",
			"SELECT * FROM building",
			"SELECT * FROM station",
			"SELECT * FROM port"
		];

		db_conn.multiple_select(select_lst, (err, results) => {
			if(err) {
				on_complete(err, null);
			} else {
				on_complete(null, {
					servers: results.recordsets[0],
					devices: results.recordsets[1],
					rack: results.recordsets[2],
					building: results.recordsets[3],
					station: results.recordsets[4],
					port: results.recordsets[5]
				});
			}
		});
		
	},

	/** 
 	* @function 'getDeviceStatusList' - Recupera i dati dalla tabella deviceStatus
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 getDeviceStatusList: function(on_complete) {

		db_conn.select("SELECT * FROM device_status", (err, result) => {
			
			if(err) {
				on_complete(err, null); 
			} else {
				on_complete(null, result)
			}        	

		}); 
	},

	/** 
 	* @function 'getDeviceStatusData' - Recupera i dati dalle tabelle device_status, streams, stream_fields, reference
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 getDeviceStatusData: function(on_complete) {

		let select_lst = [
			"SELECT * FROM device_status",
			"SELECT * FROM streams",
			"SELECT * FROM stream_fields",
			"SELECT * FROM reference"
		];

		db_conn.multiple_select(select_lst, (err, results) => {
			if(err) {
				on_complete(err, null);
			} else {
				on_complete(null, {
					devicestatus: results.recordsets[0],
					streams: results.recordsets[1],
					streamfields: results.recordsets[2],
					reference: results.recordsets[3]
				});
			}
		});		

	},


	/** 
 	* @function 'getAcksData' - Recupera i dati dalla tabella device_ack
 	* @param {callback} on_complete - function(err, result)
 	*/			
	getAcksData: function(on_complete) {

  		var ret = new Object(); 
  		db_conn.select("SELECT * FROM device_ack", (err, result) => {
			if (err) result=null;
			ret.acks = result;
			on_complete(err, ret)
		}); 		
		
	},


	/** 
 	* @function 'getEventsData' - Recupera i dati dalla tabella events
 	* @param {callback} on_complete - function(err, result)
 	*/			
	 getEventsData: function(on_complete) {

			var ret = new Object(); 
			db_conn.select("SELECT * FROM events", (err, result) => {
			if (err) result=null;
			ret.events = result;
			on_complete(err, ret)
		}); 		
		
	},

	/** 
 	* @function 'deleteEventsData' - cancella le righe dalla tabella events
 	* @param {array} inclusion - Id della tabella da eliminare
 	* @param {callback} on_complete - function(err, result)
 	*/			
	 deleteEventsData: function(inclusion, on_complete) {

		var sql= "DELETE FROM events WHERE EventID IN ("+inclusion.join(',')+")";
		//console.log("Query da eseguire: " + sql);
		//per debug
		//sql="";
		
		db_conn.delete(sql, (err, result) => {
			
    	if(err) {
    		on_complete(err, null);
    	} else {
    		on_complete(null, result)
    	}        	

		}); 
	},	

}