const config = require('../../config.js');


module.exports = (() => {

	/** 
 	* @function 'getNumRows' - Ritorna il numero di righe, tra tutte le chiavi dell'oggetto
 	* @param {object} oggetto - oggetto dal quale recuperare il numero di righe
	* @returns {int} numero di righe
 	*/
	 var _getNumRows = (oggetto) => {
		var ret=0;
		for (var key in oggetto) //per ogni tabella
		{
			ret+=oggetto[key].length;
		}
		return ret;
	}		

	/** 
 	* @function '_bufferReviver' - opzione per JSON.parse, che permette di ricreare correttamente un oggetto con all'interno dei Buffer
 	* @param {object} oggetto - oggetto dal quale recuperare il numero di righe
	* @returns {int} numero di righe
	 */
	var _bufferReviver = (key, value) => {  
		var a;  
		if (typeof value === 'object') {  
			if ((value!=null) && (value.type=="Buffer"))
			{
				return Buffer.from(value.data);
			}
		}  
		return value;  
	};  

	/** 
 	* @function 'MergeDataSet' - Crea un oggetto con le differenze tra i due dataset
 	* @param {string} configNew - versione stringify dell'oggetto preso dal db
 	* @param {string} configCache - versione stringify dell'oggetto preso dalla cache
 	* @param {array} syncDataset - array di oggetti da utilizzare per sapere quali campi cercare
	* @returns {object} Oggetto contenente il nuovo dataset per la cache e quello risultato del confronto
	*/
	var _mergeDataSet = (configNew, configCache, syncDataset) => {
		console.log("MergeDataSet");
		var configChange= new Object(); // config risultante dal confronto
		//primo passo, per ogni chiave (sarebbe una tabella del db) nel dataset, vedo se ci sono cambiamenti rispetto alla versione presa dalla cache

		for (var key in configNew) //per ogni tabella
		{
			var tabella=configNew[key];
			var tabellaCache=configCache[key];
			//console.log(tabella);
			var sColumns = _getColumnsToCompare(tabella, key, syncDataset); //prendo le colonne da confrontare, in genere tutte
			var PK = _getPK4table(key, syncDataset); //prendo la colonna chiave primaria (o le colonne, se più di una)

			for (var i=0;i<tabella.length;i++) //per ogni riga
			{
				//esiste questa riga in tabellaCache?
				var PKValue=_getPKValue(tabella[i],PK); //array di chiavi primarie/a per questa riga
				var rigaNew=tabella[i];

				var rigaCache=null;
				for (var t=0;t<tabellaCache.length;t++)
				{
					if (_checkRowByPK(tabellaCache[t],PKValue,PK)==true)
					{
						rigaCache=tabellaCache[t];
						break;
					}
				}
				
				if (rigaCache==null) //non esisteva, la devo aggiungere
				{
					//questa tabella esiste già nel configChange?
					var trovato=false;
					for (var key2 in configChange) 
					{
						if (key2==key)
						{
							//la tabella esisteva già
							trovato=true;
							configChange[key2].push(rigaNew);
							break;
						}
					}
					if (!trovato) //la tabella non era mai stata inserita
					{
						configChange[key] = new Array();
						configChange[key].push(rigaNew);
					}

					//e aggiungo questa riga nel configCache
					configCache[key].push(rigaNew);
				}
				else //esiste, controllo se se è cambiato qualche valore in qualche campo
				{
					var differenzaTrovata=false;
					for (var t=0;t<sColumns.length;t++) //colonna da confrontare
					{
						if (differenzaTrovata) break;
						//console.log(typeof rigaNew[sColumns[t]]);
						if (((typeof rigaNew[sColumns[t]]!='object') && (rigaNew[sColumns[t]]!=rigaCache[sColumns[t]])) ||
						((typeof rigaNew[sColumns[t]]=='object') && (Object.compare(rigaNew[sColumns[t]],rigaCache[sColumns[t]])==false)))  //almeno una colonna è diversa, aggiungo la riga al configChange
						{
							differenzaTrovata=true;
							console.log("Column Changed:"+key+"."+sColumns[t]);
							console.log("Vecchio valore:"+rigaCache[sColumns[t]]);
							console.log("Nuovo valore:"+rigaNew[sColumns[t]]);
							
							//questa tabella esiste già nel configChange?
							var trovato=false;
							for (var key2 in configChange) 
							{
								if (key2==key)
								{
									//la tabella esisteva già
									trovato=true;
									configChange[key2].push(rigaNew);
									break;
								}
							}
							if (!trovato) //la tabella non era mai stata inserita
							{
								configChange[key] = new Array();
								configChange[key].push(rigaNew);
							}
					
						}
					}
					//in ogni caso, la corrispettiva riga del configCache la sovrascrivo con quella del configNew
					//trovo la riga con quella PK
					for (var t=0;t<configCache[key].length;t++)
					{
						if (_checkRowByPK(configCache[key][t],PKValue,PK)==true)
						{
							configCache[key][t]=rigaNew;
						}
					}
				}
			}

			//gestioneCancellazione
			//ora ho un configCache con le righe eventualmente aggiunte, e un configChange con i soli cambiamenti da aggiungere. Ora vedo se qualcosa è stato cancellato
			tabellaCache=configCache[key]; //configCache potrebbe essere stata aggiornata con nuovi dati, la riprendo
			for (var i=0;i<tabellaCache.length;i++) //per ogni riga
			{
				//esiste questa riga in tabella? (quella presa dal db)
				var PKValue=_getPKValue(tabellaCache[i],PK); //array di chiavi primarie/a per questa riga
				var rigaCache=tabellaCache[i];

				var rigaNew=null;
				for (var t=0;t<tabella.length;t++)
				{
					if (_checkRowByPK(tabella[t],PKValue,PK)==true)
					{
						rigaNew=tabella[t];
						break;
					}
				}
				
				if (rigaNew==null) //non l'ha trovata, è stata cancellata
				{
					//questa tabella esiste già nel configChange?
					var trovato=false;
					rigaCache.grisAskRemoval=true; //per indicare che la deve rimuovere
					for (var key2 in configChange) 
					{
						if (key2==key)
						{
							//la tabella esisteva già
							trovato=true;
							configChange[key2].push(rigaCache);
							break;
						}
					}
					if (!trovato) //la tabella non era mai stata inserita
					{
						configChange[key] = new Array();
						configChange[key].push(rigaCache);
					}

					//e rimuovo questa riga dal configCache 
					for (var t=0;t<configCache[key].length;t++)
					{
						if (_checkRowByPK(configCache[key][t],PKValue,PK)==true)
						{
							configCache[key].splice(t,1);
							break;
						}
					}					
				}

			}


		}
		
		//devo restituire il risultato, configCache e configChange
		var ret=new Object();
		ret.cache = configCache;
		ret.change = configChange;
		
		return ret;


	}

	/** 
 	* @function 'getColumnsToCompare' - estrae le colonne da usare per il confronto
 	* @param {Object} tabella - tabella dalla quale estrarre i campi da confrontare
 	* @param {string} key - chiave per cercare quali campi estrarre dal syncDataset
 	* @param {array} syncDataset - array di oggetti da utilizzare per sapere quali campi cercare
	* @returns {array} array di colonne da usare per il confronto
 	*/
	 var _getColumnsToCompare = (tabella, key, syncDataset) => {
		//console.log("getColumnsToCompare");
		var ret = new Array();
		var colonne="";
		for (var i=0;i<syncDataset.length;i++)
		{
			if (syncDataset[i].name==key) colonne=syncDataset[i].compareColumn;
		}
		for (var key in tabella[0]) {
			if ((colonne.includes(key)) || (colonne=="*")) ret.push(key);
		}
		return ret;
	}

	
	/** 
 	* @function 'getPK4table' - estrae la/le colonne PK dalla tabella
 	* @param {string} key - chiave per cercare la/le colonne PK dal syncDataset
 	* @param {array} syncDataset - array di oggetti da utilizzare per sapere la/le colonne PK
	* @returns {array} array di PK
 	*/
	 var _getPK4table = (key, syncDataset) => {
		//console.log("getPK4table");
		var ret = new Array();
		for (var t=0;t<syncDataset.length;t++)
		{
			if (syncDataset[t].name==key)
			{
				ret=(syncDataset[t].pk).split(",");
				break;
			}
		}
		return ret;
	}

	/** 
 	* @function 'getPKValue' - estrae la/le PK dalla riga
 	* @param {Object} row - riga dalla quale estrarre la/le PK
 	* @param {array} pk - array di pk per cercare il/i valori
	* @returns {array} array di valori di PK
 	*/
	 var _getPKValue = (row, pk) => {
		//console.log("getPKValue");
		var ret = new Array();
		for (var t=0;t<pk.length;t++)
		{
			ret.push(row[pk[t]]);
		}
		return ret;
	}
	

	/** 
 	* @function 'checkRowByPK' - verifica che la riga sia identificata dagli stessi valori di PK (è la medesima riga...)
 	* @param {Object} row - riga su cui operare il confronto
	* @param {array} PKValue - array di valori pk per effettuare il confronto
	* @param {array} PK - array di valori pk per effettuare il confronto
	* @returns {boolean} true/false se è la medesima riga
 	*/
	 var _checkRowByPK = (row, PKValue, PK) => {
		//console.log("checkRowByPK");
		var ret = false;
		var numOk=0;
		for (var t=0;t<PK.length;t++)
		{
			if (row[PK[t]]==PKValue[t]) numOk++;
		}
		if (numOk==PK.length) ret=true;
		return ret;
	}



	return {
		getNumRows:_getNumRows,
		bufferReviver:_bufferReviver,
		mergeDataSet:_mergeDataSet
	}


})();