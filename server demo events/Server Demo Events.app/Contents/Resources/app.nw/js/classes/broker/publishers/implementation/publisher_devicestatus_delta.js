"use strict";
var zlib = require('zlib');
const iPublisher = require('../iPublisher.js');


class publisher_devicestatus_delta extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_DEVICESTATUS_DELTA',
	    	topic: '/devicestatus/delta'
	    });
	}

	publish(client, message) {
		
		console.log("PUBLISHER_DEVICESTATUS_DELTA Invio messaggio.....");
		let _msg = JSON.stringify(message);
		let compressed = zlib.gzipSync(_msg);
		super.publish(client, compressed);
		super.writeTestFile(message);		
		super.sentToRestService(compressed, true);		
		super.aggiornaDataOra();		
	}
	

}

module.exports = publisher_devicestatus_delta;