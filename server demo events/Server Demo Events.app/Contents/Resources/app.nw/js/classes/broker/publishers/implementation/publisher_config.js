"use strict";
const iPublisher = require('../iPublisher.js');


class publisher_external_event extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_EXTERNAL_EVENT',
	    	topic: '/external_event'
	    });
	}

	publish(client, message) {
		
		console.log("PUBLISHER_EXTERNAL_EVENT Invio messaggio.....");
		super.publish(client, message);
	
	}
	

}

module.exports = publisher_external_event;